#!/usr/bin/env python3
import os
import sys

file = str(sys.argv[1])
description = str(sys.argv[2])
disk_img = "emulate_test.img"
disk_img_sz = 65536 * 4096

fd = os.open(disk_img, os.O_CREAT)
os.close(fd)

os.truncate(disk_img, disk_img_sz)
os.system("mkfs.hmnxfs -b 4096 " + disk_img)

os.execv("./" + file, [file, disk_img])
