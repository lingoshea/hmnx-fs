#include <emulated/eblock.h>
#include <emulated/ebuffer.h>
#include <shs/hmnx_error.h>
#include <shs/device.h>
#include <fs.h>
#include <iostream>
#include <cstring>
#include <inode.h>
#include <shs/hmnx_time.h>
#include <iomanip>
#include <shs/sbuffer.h>


void print_sb_info(super_block_t sb)
{
    printf("Filesystem basic information:\n"
           "      Super block structure size:         %lu bytes\n"
           "   *  Magic number:                       0x%lX\n"
           "   *  %02dKb block count:                    %lu\n"
           "  ==  Journaling starts from:             %lu\n"
           "      %02dKb journaling block count:        %lu\n"
           "  ==  Minibitmap starts from:             %lu\n"
           "      %02dKb Minibitmap block count:        %lu\n"
           "  ==  Bitmap starts from:                 %lu\n"
           "      %02dKb bitmap block count :           %lu\n"
           "  ==  Snapshot history starts from:       %lu\n"
           "      %02dKb snapshot history block count:  %lu\n"
           "  ==  Logic block starts from:            %lu\n"
           "      Logic block count:                  %lu\n"
           "      Logic block size:                   %lu\n"
           "  **  Logic space efficiency:             %lf\n"
           "  **  Filesystem space efficiency:        %lf\n",
           sizeof (sb),
           sb.magic,
           BLOCK_SIZE / 1024,
           sb.filesystem_block_count,
           sb.filesystem_journal_starting_block,
           BLOCK_SIZE / 1024,
           sb.filesystem_journal_block_count,
           sb.mini_bitmap_starting_block,
           BLOCK_SIZE / 1024,
           sb.mini_bitmap_block_count,
           sb.bitmap_starting_block,
           BLOCK_SIZE / 1024,
           sb.bitmap_block_count,
           sb.snapshot_history_starting_block,
           BLOCK_SIZE / 1024,
           sb.snapshot_history_block_count,
           sb.logic_block_starting_block,
           sb.logic_block_count,
           sb.logic_block_size,
           (double)(sb.logic_block_size * sb.logic_block_count)
           / (double)sb.filesystem_block_count * 100.00,
           (double)(sb.logic_block_size * sb.logic_block_count
                    + sb.logic_block_starting_block)
           / (double)sb.filesystem_block_count * 100.00);
}

int main(int, char ** argv)
{
    try
    {
        set_limit(1024 * 1024 * 1024 * 64L);

        device filesystem;
        filesystem.set_bpoll_sz(8);
        filesystem.open(argv[1]);

        journal _fs_journal(filesystem._file_info.fd, 0, 1);
        filesystem.add_fs_journal(&_fs_journal);

        hmnx_inode root_inode = {
                .mode = S_IFDIR | 0755,
                .access_time = current_time(),
                .change_time = current_time(),
                .modify_time = current_time(),
                .file_len = 3 * sizeof (dentry_t),
                .inode_links = 2,
                .gid = 0,
                .uid = 0,
                .inode_direct_block_residence = 1,
                .inode_indirect_block_residence = 1,
        };

        eblock _block_pool(filesystem,
                          _fs_journal,
                          1, 1,
                          2, 1,
                          3, 1,
                          4, 28, 1
                          );

        inode_pool _fsinode_pool(filesystem, _block_pool, _fs_journal);

        block_attribute_t dull_attribute { .block_type = BLOCK_INODE };

        for (uint64_t j = 0; j < 100; j++)
        {
            for (uint64_t i = 1; i <= 28; i++)
            {
                _fsinode_pool.write_inode(root_inode, i);
                _block_pool.block_bitmap.set_occupation_status(true, i);
                _block_pool.block_bitmap.write_attribute(dull_attribute, i);
            }
        }

        for (uint64_t j = 0; j < 100; j++)
        {
            for (uint64_t i = 1; i <= 28; i++)
            {
                if (!_block_pool.block_bitmap.if_occupied(i))
                {
                    return EXIT_FAILURE;
                }

                auto inode = _fsinode_pool.read_inode(i);
                if (!!memcmp(&inode, &root_inode, sizeof(hmnx_inode)))
                {
                    return EXIT_FAILURE;
                }

                auto attr = _block_pool.block_bitmap.read_attribute(i);
                if (!!memcmp(&attr, &dull_attribute, sizeof(dull_attribute)))
                {
                    return EXIT_FAILURE;
                }
            }
        }

        return EXIT_SUCCESS;
    }
    catch (hmnx_error_t & err)
    {
        std::cout << err.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        return 1;
    }
}
