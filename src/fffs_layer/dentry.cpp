#include <fs.h>
#include <inode.h>
#include <shs/hmnx_error.h>

uint64_t hmnx::remove_dentry_from_inode(inode_filed_io & _inode, const std::string & target_name)
{
    dentry_t dentry_cmp{};
    for (uint64_t i = 0; i < _inode.size() / sizeof (dentry_t); i++)
    {
        _inode.read((char*)&dentry_cmp, sizeof (dentry_cmp), i * sizeof (dentry_cmp));
        if (!strncmp(target_name.c_str(), dentry_cmp.name, sizeof (dentry_cmp.name)))
        {
            uint64_t inode_num = dentry_cmp.inode_logic_location;
            memset(&dentry_cmp, 0, sizeof (dentry_cmp));
            _inode.write((char*)&dentry_cmp, sizeof (dentry_cmp), i * sizeof (dentry_cmp), false);
            return inode_num;
        }
    }

    errno = ENOENT;
    str_throw_hmnx_error("No such file or directory");
}

void hmnx::add_dentry_in_inode(inode_filed_io & inode, dentry_t & dentry)
{
    dentry_t det_cmp{};
    for (uint64_t i = 0; i < inode.size() / sizeof (dentry); i++)
    {
        inode.read((char*)&det_cmp, sizeof (det_cmp), i * sizeof (det_cmp));
        if (!strncmp(dentry.name, det_cmp.name, sizeof (det_cmp.name)))
        {
            errno = EEXIST;
            code_throw_hmnx_error(ATTEMPT_TO_ADD_DUPLICATED_DENTRY);
        }
    }

    inode.write((char *) &dentry, sizeof(dentry), inode.size());
}
