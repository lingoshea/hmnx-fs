#include <fs.h>
#include <inode.h>
#include <shs/hmnx_error.h>
#include <emulated/eblock.h>
#include <shs/journal.h>
#include <shs/device.h>

uint64_t find_inode_in_dentry(const std::string & str, inode_filed_io & inode_io)
{
    dentry_t dentry{};
    for (uint64_t i = 0; i < inode_io.size() / sizeof (dentry); i++)
    {
        inode_io.read((char*)&dentry, sizeof (dentry), sizeof (dentry) * i);
        if (dentry.inode_logic_location != 0
            && str == dentry.name)
        {
            return dentry.inode_logic_location;
        }
    }

    return 0;
}

uint64_t hmnx::namei(path_t path, inode_pool & _inode_pool, eblock & eblock_pool)
{
    if (!path.size())
    {
        return ROOT_DIR_INODE_NUM;
    }

    uint64_t inode = ROOT_DIR_INODE_NUM;

    for (auto & dir : path)
    {
        auto i_inode = _inode_pool.read_inode(inode);
        auto i_inode_io = inode_filed_io(i_inode, eblock_pool);
        inode = find_inode_in_dentry(dir, i_inode_io);
        if (inode == 0)
        {
            errno = ENOENT;
            str_throw_hmnx_error("No such file or directory");
        }
    }

    return inode;
}