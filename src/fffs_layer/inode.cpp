#include <inode.h>
#include <shs/hmnx_error.h>
#include <shs/hmnx_time.h>
#include <fs.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define BLK_ENTRY_NUM_PER_BLOCK             (BLOCK_SIZE / sizeof(uint64_t))
#define FIRST_LEVEL_OFFSET_STOPPING_POINT   (INMAP_FIRST_LEVEL_FILED_ENTRY_COUNT - 1)
#define SECND_LEVEL_OFFSET_STOPPING_POINT   (FIRST_LEVEL_OFFSET_STOPPING_POINT \
                                            + INMAP_SECND_LEVEL_FILED_ENTRY_COUNT * BLK_ENTRY_NUM_PER_BLOCK - 1)
#define THIRD_LEVEL_OFFSET_STOPPING_POINT   (SECND_LEVEL_OFFSET_STOPPING_POINT \
                                            + INMAP_THIRD_LEVEL_FILED_ENTRY_COUNT * BLK_ENTRY_NUM_PER_BLOCK \
                                            * BLK_ENTRY_NUM_PER_BLOCK - 1)

hmnx_inode inode_pool::read_inode(uint64_t block_num)
{
    hmnx_inode inode{};
    auto & ebuffer = eblock_pool.access(block_num);
    ebuffer.read((char*)&inode, sizeof (inode), 0);
    return inode;
}

void inode_pool::write_inode(hmnx_inode &inode, uint64_t block_num)
{
    fs_journal.command(MODIFY_INODE_CONT, GET_H32_OF_UINT64(block_num), GET_L32_OF_UINT64(block_num));

    auto & ebuffer = eblock_pool.access(block_num);
    ebuffer.write((char*)&inode, sizeof (inode), 0);

    fs_journal.command(OPERATION_FINISHED);
}

uint64_t inode_pool::alloc()
{
    fs_journal.command(ALLOCATE_INODE);
    uint64_t block_num = eblock_pool.block_bitmap.next_avail_logic_block_num();
    fs_journal.command(_PUSH_BLOCK_NUMBER, GET_H32_OF_UINT64(block_num), GET_L32_OF_UINT64(block_num));

    block_attribute_t attr =
            {
            .block_type = BLOCK_DATA,
            .block_fs_flag = BLOCK_INODE
            };
    eblock_pool.block_bitmap.write_attribute(attr, block_num);
    eblock_pool.block_bitmap.set_occupation_status(true, block_num);

    fs_journal.command(OPERATION_FINISHED);

    hmnx::inode_count_inc(filesystem);

    return block_num;
}

void inode_pool::del_inode(uint64_t block_num)
{
    fs_journal.command(DEALLOCATE_INODE, MK_64bit_PARAM(block_num));

    auto attr = eblock_pool.block_bitmap.read_attribute(block_num);
    if (attr.block_fs_flag != BLOCK_INODE || attr.block_type != BLOCK_DATA)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NOT_AN_INODE_BLOCK));
        code_throw_hmnx_error(NOT_AN_INODE_BLOCK);
    }

    attr.block_type = BLOCK_UNUSED;
    eblock_pool.block_bitmap.write_attribute(attr, block_num);
    eblock_pool.block_bitmap.set_occupation_status(false, block_num);

    hmnx::inode_count_dec(filesystem);

    fs_journal.command(OPERATION_FINISHED);
}

size_t inode_filed_io::read(char * buffer, uint64_t length, uint64_t offset)
{
    refresh_inode();

    if (my_inode.locked == (uint64_t)INODE_LOCKED_READ)
    {
        return 0;
    }

    if (!my_inode.file_len)
    {
        return 0;
    }

    if (offset > my_inode.file_len)
    {
        return 0;
    }

    if (offset + length > my_inode.file_len)
    {
        length = my_inode.file_len - offset;
    }

    uint64_t read_off = 0;

    uint64_t starting_block = offset / (eblock_pool.logic_block_size * BLOCK_SIZE);
    uint64_t starting_block_offset = offset % (eblock_pool.logic_block_size * BLOCK_SIZE);
    uint64_t starting_block_ops_len = MIN(eblock_pool.logic_block_size * BLOCK_SIZE - starting_block_offset,
                                          length);
    uint64_t full_block_count = (length - starting_block_ops_len) / (eblock_pool.logic_block_size * BLOCK_SIZE);
    uint64_t last_block_ops_len = (length - starting_block_ops_len) % (eblock_pool.logic_block_size * BLOCK_SIZE);

    uint64_t starting_block_eb = inode_query_logic_block_entry(starting_block);
    auto & starting_block_buffer = eblock_pool.access(starting_block_eb);
    read_off = starting_block_buffer.read(buffer, starting_block_ops_len, (int64_t)starting_block_offset);

    for (uint64_t i = 1; i <= full_block_count; i++)
    {
        uint64_t full_block_eb = inode_query_logic_block_entry(starting_block + i);
        auto & full_block_buffer = eblock_pool.access(full_block_eb);
        read_off += full_block_buffer.read(buffer + read_off,
                                           eblock_pool.logic_block_size * BLOCK_SIZE,
                                           0);
    }

    if (last_block_ops_len)
    {
        uint64_t last_block_eb = inode_query_logic_block_entry(starting_block + full_block_count + 1);
        auto & last_buffer = eblock_pool.access(last_block_eb);
        read_off += last_buffer.read(buffer + read_off, last_block_ops_len, 0);
    }

    my_inode.access_time = current_time();
    update_inode();

    return read_off;
}

size_t inode_filed_io::write(const char * buffer, uint64_t length, uint64_t offset, bool require_resize)
{
    refresh_inode();

    if (my_inode.locked == (uint64_t)INODE_LOCKED_WRITE)
    {
        return 0;
    }

    if (offset > my_inode.file_len)
    {
        return 0;
    }

    if (require_resize)
    {
        resize(offset + length);
    }
    else
    {
        if (offset + length > my_inode.file_len)
        {
            length = my_inode.file_len - offset;
        }
    }

    uint64_t write_off = 0;

    uint64_t starting_block = offset / (eblock_pool.logic_block_size * BLOCK_SIZE);
    uint64_t starting_block_offset = offset % (eblock_pool.logic_block_size * BLOCK_SIZE);
    uint64_t starting_block_ops_len = MIN(eblock_pool.logic_block_size * BLOCK_SIZE - starting_block_offset,
                                          length);
    uint64_t full_block_count = (length - starting_block_ops_len) / (eblock_pool.logic_block_size * BLOCK_SIZE);
    uint64_t last_block_ops_len = (length - starting_block_ops_len) % (eblock_pool.logic_block_size * BLOCK_SIZE);

    uint64_t starting_block_eb = inode_query_logic_block_entry(starting_block);
    auto & starting_block_buffer = eblock_pool.access(starting_block_eb);
    write_off = starting_block_buffer.write(buffer, starting_block_ops_len, (int64_t)starting_block_offset);

    for (uint64_t i = 1; i <= full_block_count; i++)
    {
        uint64_t full_block_eb = inode_query_logic_block_entry(starting_block + i);
        auto & full_block_buffer = eblock_pool.access(full_block_eb);
        write_off += full_block_buffer.write(buffer + write_off,
                                             eblock_pool.logic_block_size * BLOCK_SIZE,
                                             0);
    }

    if (last_block_ops_len)
    {
        uint64_t last_block_eb = inode_query_logic_block_entry(starting_block + full_block_count + 1);
        auto & last_buffer = eblock_pool.access(last_block_eb);
        write_off += last_buffer.write(buffer + write_off, last_block_ops_len, 0);
    }

    my_inode.modify_time = current_time();
    update_inode();

    return write_off;
}

uint64_t inode_filed_io::inode_query_logic_block_entry(uint64_t offset)
{
    if (offset <= FIRST_LEVEL_OFFSET_STOPPING_POINT)
    {
        return my_inode.file_zone[offset];
    }
    else if (offset <= SECND_LEVEL_OFFSET_STOPPING_POINT)
    {
        uint64_t block_list_starting = (offset - FIRST_LEVEL_OFFSET_STOPPING_POINT - 1) / BLK_ENTRY_NUM_PER_BLOCK;
        uint64_t block_list_off = (offset - FIRST_LEVEL_OFFSET_STOPPING_POINT - 1) % BLK_ENTRY_NUM_PER_BLOCK;

        return get_block_num_by_off(
                my_inode.file_zone[FIRST_LEVEL_OFFSET_STOPPING_POINT + 1 + block_list_starting],
                block_list_off);
    }
    else if (offset <= THIRD_LEVEL_OFFSET_STOPPING_POINT)
    {
        uint64_t blk_1x_off = offset / (BLK_ENTRY_NUM_PER_BLOCK * BLK_ENTRY_NUM_PER_BLOCK);
        uint64_t blk_2x_ops_count = offset % (BLK_ENTRY_NUM_PER_BLOCK * BLK_ENTRY_NUM_PER_BLOCK);
        uint64_t blk_2x_iboff = blk_2x_ops_count / BLK_ENTRY_NUM_PER_BLOCK;
        uint64_t blk_2x_rdoff = blk_2x_ops_count % BLK_ENTRY_NUM_PER_BLOCK;

        uint64_t blk_1x_blk_num = my_inode.file_zone[blk_1x_off];
        uint64_t blk_2x_blk_num = get_block_num_by_off(blk_1x_blk_num, blk_2x_iboff);
        uint64_t block_num = get_block_num_by_off(blk_2x_blk_num, blk_2x_rdoff);
        return block_num;
    }
    else
    {
        return 0;
    }
}

void inode_filed_io::resize(size_t tag_size)
{
//    refresh_inode();

    if (tag_size == my_inode.file_len)
    {
        return;
    }

    uint64_t target_block_count = tag_size / (eblock_pool.logic_block_size * BLOCK_SIZE) +
            (tag_size % (eblock_pool.logic_block_size * BLOCK_SIZE) == 0 ? 0 : 1);
    uint64_t my_block_count = size() / (eblock_pool.logic_block_size * BLOCK_SIZE) +
            (size() % (eblock_pool.logic_block_size * BLOCK_SIZE) == 0 ? 0 : 1);

    if (target_block_count == my_block_count)
    {
        ;
    }
    else if (target_block_count > my_block_count)
    {
        for (uint64_t i = 0; i < target_block_count - my_block_count; i++)
        {
            add_block();
        }
    }
    else
    {
        for (uint64_t i = 0; i < my_block_count - target_block_count; i++)
        {
            del_block();
        }
    }

    my_inode.file_len = tag_size;
    update_inode();
}

void inode_filed_io::del_block()
{
    uint64_t current_block_count = size() / (eblock_pool.logic_block_size * BLOCK_SIZE) +
                                   (size() % (eblock_pool.logic_block_size * BLOCK_SIZE) == 0 ? 0 : 1);

    if (current_block_count <= FIRST_LEVEL_OFFSET_STOPPING_POINT)
    {
        dealloc_at_first_level(current_block_count);
        return;
    }
    else if (current_block_count > FIRST_LEVEL_OFFSET_STOPPING_POINT
             && current_block_count <= SECND_LEVEL_OFFSET_STOPPING_POINT)
    {
        dealloc_at_second_level(current_block_count);
    }
    else if (current_block_count > SECND_LEVEL_OFFSET_STOPPING_POINT
             && current_block_count <= THIRD_LEVEL_OFFSET_STOPPING_POINT)
    {
        dealloc_at_third_level(current_block_count);
    }
    else
    {
        str_throw_hmnx_error("Filesystem exhausted all available filed entries!");
    }
}

void inode_filed_io::add_block()
{
    uint64_t current_block_count = size() / (eblock_pool.logic_block_size * BLOCK_SIZE) +
                                   (size() % (eblock_pool.logic_block_size * BLOCK_SIZE) == 0 ? 0 : 1);

    if (current_block_count <= FIRST_LEVEL_OFFSET_STOPPING_POINT)
    {
        alloc_at_first_level(current_block_count);
        return;
    }
    else if (current_block_count > FIRST_LEVEL_OFFSET_STOPPING_POINT
          && current_block_count <= SECND_LEVEL_OFFSET_STOPPING_POINT)
    {
        alloc_at_second_level(current_block_count);
    }
    else if (current_block_count > SECND_LEVEL_OFFSET_STOPPING_POINT
             && current_block_count <= THIRD_LEVEL_OFFSET_STOPPING_POINT)
    {
        alloc_at_third_level(current_block_count);
    }
    else
    {
        str_throw_hmnx_error("Filesystem exhausted all available filed entries!");
    }
}

void inode_filed_io::alloc_at_first_level(uint64_t offset)
{
    my_inode.file_zone[offset] = simple_alloc_blk();
}

void inode_filed_io::dealloc_at_first_level(uint64_t offset)
{
    simple_dealloc_blk(my_inode.file_zone[offset]);
    my_inode.file_zone[offset] = 0;
}

void inode_filed_io::alloc_at_second_level(uint64_t offset /* allocate position */)
{
    offset -= FIRST_LEVEL_OFFSET_STOPPING_POINT;
    uint64_t inode_filed_entry_offset = offset / BLK_ENTRY_NUM_PER_BLOCK
            + offset % BLK_ENTRY_NUM_PER_BLOCK == 0 ? 0 : 1;
    uint64_t in_block_entry_offset = offset % BLK_ENTRY_NUM_PER_BLOCK - 1;
    uint64_t block_entry = my_inode.file_zone[FIRST_LEVEL_OFFSET_STOPPING_POINT + inode_filed_entry_offset];
    if (block_entry == 0)
    {
        block_entry = simple_alloc_blk();
        my_inode.file_zone[FIRST_LEVEL_OFFSET_STOPPING_POINT + inode_filed_entry_offset] = block_entry;
    }

    alloc_in_blk_entry(block_entry, in_block_entry_offset);
}

void inode_filed_io::dealloc_at_second_level(uint64_t offset)
{
    offset -= FIRST_LEVEL_OFFSET_STOPPING_POINT;
    uint64_t inode_filed_entry_offset = offset / BLK_ENTRY_NUM_PER_BLOCK
                                        + offset % BLK_ENTRY_NUM_PER_BLOCK == 0 ? 0 : 1;
    uint64_t in_block_entry_offset = offset % BLK_ENTRY_NUM_PER_BLOCK - 1;
    uint64_t block_entry = my_inode.file_zone[FIRST_LEVEL_OFFSET_STOPPING_POINT + inode_filed_entry_offset];

    dealloc_in_blk_entry(block_entry, in_block_entry_offset);
}

void inode_filed_io::alloc_at_third_level(uint64_t starting)
{
    str_throw_hmnx_error("Function not implemented!");
}

void inode_filed_io::dealloc_at_third_level(uint64_t starting)
{
    str_throw_hmnx_error("Function not implemented!");
}


uint64_t inode_filed_io::simple_alloc_blk()
{
    auto new_block = eblock_pool.block_bitmap.next_avail_logic_block_num();
    eblock_pool.block_bitmap.set_occupation_status(true, new_block);
    block_attribute_t attr = { .block_type = BLOCK_DATA };
    eblock_pool.block_bitmap.write_attribute(attr, new_block);
    return new_block;
}

void inode_filed_io::simple_dealloc_blk(uint64_t blk)
{
    eblock_pool.block_bitmap.set_occupation_status(false, blk);
    block_attribute_t attr = { };
    eblock_pool.block_bitmap.write_attribute(attr, blk);
}

uint64_t inode_filed_io::get_block_num_by_off(uint64_t blk, uint64_t off)
{
    uint64_t ret;
    eblock_pool.access(blk).read((char*)&ret, sizeof (ret), off * sizeof (ret));
    return ret;
}

void inode_filed_io::dealloc_in_block(uint64_t blk, uint64_t starting, uint64_t len)
{
    auto & buffer = eblock_pool.access(blk);
    for (uint64_t i = 0; i < len; i++)
    {
        uint64_t blk_num;
        buffer.read((char*)&blk_num, sizeof (blk_num), (i + starting) * sizeof (blk_num));
        simple_dealloc_blk(blk_num);
    }
}

void inode_filed_io::alloc_in_block(uint64_t blk, uint64_t starting, uint64_t len)
{
    auto & buffer = eblock_pool.access(blk);
    for (uint64_t i = 0; i < len; i++)
    {
        uint64_t blk_num = simple_alloc_blk();
        buffer.write((char*)&blk_num, sizeof (blk_num), (i + starting) * sizeof (blk_num));
    }
}

void inode_filed_io::write_block_num_by_off(uint64_t blk_num, uint64_t blk, uint64_t off)
{
    eblock_pool.access(blk_num).write((char*)&blk, sizeof (blk), sizeof (blk) * off);
}

void inode_filed_io::update_inode()
{
    eblock_pool.access(my_inode.inode_direct_block_residence).write(
            (char *) &my_inode, sizeof(my_inode), 0);

}

void inode_filed_io::refresh_inode()
{
    eblock_pool.access(my_inode.inode_direct_block_residence).read(
            (char*)&my_inode, sizeof (my_inode), 0);
}

void inode_filed_io::alloc_in_blk_entry(uint64_t blk, uint64_t off)
{
    auto & buffer = eblock_pool.access(blk);
    uint64_t new_blk = simple_alloc_blk();
    buffer.write((char*)&new_blk, sizeof (new_blk), off * sizeof (new_blk));
}

void inode_filed_io::dealloc_in_blk_entry(uint64_t blk_num, uint64_t off)
{
    uint64_t blk;
    auto & buffer = eblock_pool.access(blk_num);

    buffer.read((char*)&blk, sizeof (blk), off * sizeof (blk));
    simple_dealloc_blk(blk);

    blk = 0;
    buffer.write((char*)&blk, sizeof (blk), off * sizeof (blk));

    if (off == 0)
    {
        simple_dealloc_blk(blk_num);
    }
}
