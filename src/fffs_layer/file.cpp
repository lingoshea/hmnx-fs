#include <fs.h>
#include <shs/device.h>
#include <shs/hmnx_error.h>

void hmnx::eblock_count_inc(device & root_dev)
{
    super_block_t sb {};
    auto & sb_buffer = root_dev.alloc_buffer(0);
    sb_buffer.read((char*)&sb, sizeof (sb), 0);
    sb.allocated_logic_block_count += 1;
    sb_buffer.write((char*)&sb, sizeof (sb), 0);
}

void hmnx::eblock_count_dec(device & root_dev)
{
    super_block_t sb {};
    auto & sb_buffer = root_dev.alloc_buffer(0);
    sb_buffer.read((char*)&sb, sizeof (sb), 0);
    if (sb.allocated_logic_block_count != 0)
    {
        sb.allocated_logic_block_count -= 1;
    }
    else
    {
        code_throw_hmnx_error(METADATA_CORRUPTION_DETECTED);
    }
    sb_buffer.write((char*)&sb, sizeof (sb), 0);
}


void hmnx::inode_count_inc(device & root_dev)
{
    super_block_t sb {};
    auto & sb_buffer = root_dev.alloc_buffer(0);
    sb_buffer.read((char*)&sb, sizeof (sb), 0);
    sb.allocated_inode_count += 1;
    sb_buffer.write((char*)&sb, sizeof (sb), 0);
}

void hmnx::inode_count_dec(device & root_dev)
{
    super_block_t sb {};
    auto & sb_buffer = root_dev.alloc_buffer(0);
    sb_buffer.read((char*)&sb, sizeof (sb), 0);
    if (sb.allocated_inode_count != 0)
    {
        sb.allocated_inode_count -= 1;
    }
    else
    {
        code_throw_hmnx_error(METADATA_CORRUPTION_DETECTED);
    }
    sb_buffer.write((char*)&sb, sizeof (sb), 0);
}
