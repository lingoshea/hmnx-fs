#include <path_t.h>
#include <shs/hmnx_error.h>
#include <cerrno>

std::string path_t::operator[](unsigned int n)
{
    try
    {
        if (_path.size() > n)
        {
            return _path[n];
        }

        errno = ENOENT;
        str_throw_hmnx_error("No such file or directory");
    }
    catch (...)
    {
        throw;
    }
}

path_t::path_t(std::string fuse_path)
{
    try
    {
        std::string buffer;
        size_t last_loc = 0;

        size_t pos;
        while ((pos = fuse_path.find('/')) != std::string::npos)
        {
            buffer = fuse_path.substr(last_loc, pos);
            if (!buffer.empty())
            {
                _path.emplace_back(buffer);
            }

            pos += 1; // delete '/'
            fuse_path = fuse_path.substr(pos);
        }

        if (!fuse_path.empty())
        {
            _path.emplace_back(fuse_path);
        }
    }
    catch (...)
    {
        throw ;
    }
}
