#define FUSE_USE_VERSION 31
#include <iostream>
#include <fs.h>
#include <fuse.h>
#include <cstring>
#include <shs/hmnx_error.h>
#include <shs/device.h>
#include <shs/sbuffer.h>
#include <emulated/eblock.h>
#include <inode.h>
#include <shs/hmnx_time.h>
#include <vector>
#include <string>
#include <dlfcn.h>
#include <cstdlib>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

typedef int (*notify_init_t)(const char*);
typedef void * (*notify_notification_new_t) (const char *, const char *, const char *);
typedef void (*notify_notification_set_timeout_t)(void *, int);
typedef int (*notify_notification_show_t)(void *, void**);
notify_init_t notify_init = nullptr;
notify_notification_new_t notify_notification_new = nullptr;
notify_notification_set_timeout_t notify_notification_set_timeout = nullptr;
notify_notification_show_t notify_notification_show = nullptr;
void * libnotify_handler = nullptr;

static struct fuse_operations hmnx_ops =
        {
                .getattr    = hmnx_getattr,
                .readlink   = hmnx_readlink,
                .mknod      = hmnx_mknod,
                .mkdir      = hmnx_mkdir,
                .unlink     = hmnx_unlink,
                .rmdir      = hmnx_rmdir,
                .symlink    = hmnx_symlink,
                .rename     = hmnx_rename,
                .link       = hmnx_link,
                .chmod      = hmnx_chmod,
                .chown      = hmnx_chown,
                .truncate   = hmnx_truncate,
                .open       = hmnx_open,
                .read       = hmnx_read,
                .write      = hmnx_write,
                .statfs     = hmnx_statfs,
                .flush      = hmnx_flush,
                .release    = hmnx_release,
                .fsync      = hmnx_fsync,
//                .setxattr   = hmnx_setxattr,
//                .getxattr   = hmnx_getxattr,
//                .listxattr  = hmnx_listxattr,
//                .removexattr= hmnx_removexattr,
                .opendir    = hmnx_open,
                .readdir    = hmnx_readdir,
                .releasedir = hmnx_release,
                .fsyncdir   = hmnx_fsyncdir,
                .init       = hmnx_init,
                .destroy    = hmnx_destroy,
                .access     = hmnx_access,
                .create     = hmnx_create,
                .lock       = hmnx_lock,
                .utimens    = hmnx_utimens,
                .ioctl      = hmnx_ioctl,
                .fallocate  = hmnx_fallocate
        };

std::string lltostr_with_scale(uint64_t);

void print_sb_info(super_block_t sb)
{
    uint64_t _fs_size, _journaling_size, _minimap_size, _bitmap_size, _snapshot_history_size, _logic_size;
    uint64_t sig_block_size = BLOCK_SIZE / 1024;

    // the following size is in Kb
    _fs_size                = sb.filesystem_block_count * sig_block_size;
    _journaling_size        = sb.filesystem_journal_block_count * sig_block_size;
    _minimap_size           = sb.mini_bitmap_block_count * sig_block_size;
    _bitmap_size            = sb.bitmap_block_count * sig_block_size;
    _snapshot_history_size  = sb.snapshot_history_block_count * sig_block_size;
    _logic_size             = sb.logic_block_count * sb.logic_block_size * sig_block_size;

    printf("Filesystem basic information:\n"
           "      Super block structure size:         \t%lu bytes\n"
           "   *  Magic number:                       \t0x%lX\n"
           "   *  %1luKb block count:                    \t%lu (%s)\n"
           "  ==  Journaling starts from:             \t%lu\n"
           "      %1luKb journaling block count:         \t%lu (%s)\n"
           "  ==  Minibitmap starts from:             \t%lu\n"
           "      %1luKb Minibitmap block count:         \t%lu (%s)\n"
           "  ==  Bitmap starts from:                 \t%lu\n"
           "      %1luKb bitmap block count:             \t%lu (%s)\n"
           "  ==  Snapshot history starts from:       \t%lu\n"
           "      %1luKb snapshot history block count:   \t%lu (%s)\n"
           "  ==  Logic block starts from:            \t%lu\n"
           "      Logic block count:                  \t%lu (%s)\n"
           "      Logic block size (in %dKb):         \t%lu\n"
           "  **  Logic space efficiency:             \t%lf %%\n"
           "  **  Filesystem space efficiency:        \t%lf %%\n",
           sizeof (sb),
           sb.magic,
           sig_block_size,
           sb.filesystem_block_count, lltostr_with_scale(_fs_size).c_str(),
           sb.filesystem_journal_starting_block, sig_block_size,
           sb.filesystem_journal_block_count, lltostr_with_scale(_journaling_size).c_str(),
           sb.mini_bitmap_starting_block, sig_block_size,
           sb.mini_bitmap_block_count, lltostr_with_scale(_minimap_size).c_str(),
           sb.bitmap_starting_block, sig_block_size,
           sb.bitmap_block_count, lltostr_with_scale(_bitmap_size).c_str(),
           sb.snapshot_history_starting_block, sig_block_size,
           sb.snapshot_history_block_count, lltostr_with_scale(_snapshot_history_size).c_str(),
           sb.logic_block_starting_block,
           sb.logic_block_count, lltostr_with_scale(_logic_size).c_str(),
           (ALIGNED_SIZE) / 1024,
           sb.logic_block_size,
           (double)(sb.logic_block_size * sb.logic_block_count)
           / (double)sb.filesystem_block_count * 100.00,
           (double)(sb.logic_block_size * sb.logic_block_count
                    + sb.logic_block_starting_block)
           / (double)sb.filesystem_block_count * 100.00);
}

std::string lltostr_with_scale(uint64_t num)
{
    static char buffer [BLOCK_SIZE] { };
    std::string suffix = " ";
    uint64_t scale = 1;

    if (num < 1024)
    {
        suffix += "Kb";
    }
    else if (num < 1024 * 1024)
    {
        scale = 1024;
        suffix += "Mb";
    }
    else if (num < 1024 * 1024 * 1024)
    {
        scale = 1024 * 1024;
        suffix += "Gb";
    }
    else
    {
        scale = 1024 * 1024 * 1024;
        suffix += "Tb";
    }

    sprintf(buffer, "%.2Lf%s", num / ((long double)scale), suffix.c_str());

    return std::string(buffer);
}

static void usage(const char *progname)
{
    printf(
            "usage: %s device mountpoint [options]\n"
            "\n"
            "general options:\n"
            "    -o opt,[opt...]        Mount options.\n"
            "    -h, --help             Print help.\n"
            "    -V, --version          Print version.\n"
            "    -m, --buffer-mem       Specify buffer space memory size (in %dKb).\n"
            "    -d, --direct-io        Bypass system cache.\n"
            "    --allow-other          Allow other users access mount point.\n"
            "    --no-notification      Disable GUI notification\n"
            "\n", progname, BLOCK_SIZE / 1024);
}

#define PACKAGE_NAME    "FUSE-based HMNX Filesystem"
#define PACKAGE_VERSION GENERAL_VERSION

enum {
    KEY_VERSION,
    KEY_HELP,
    KEY_POOL_SZ,
    KEY_ALLOW_OTHER,
    KEY_DIRECT_IO,
    KEY_NO_LIBNOTIFY,
};

static struct fuse_opt hmnx_opts[] = {
        FUSE_OPT_KEY("-V",              KEY_VERSION),
        FUSE_OPT_KEY("--version",       KEY_VERSION),
        FUSE_OPT_KEY("-h",              KEY_HELP),
        FUSE_OPT_KEY("--help",          KEY_HELP),
        FUSE_OPT_KEY("-m ",             KEY_POOL_SZ),
        FUSE_OPT_KEY("--buffer-mem ",   KEY_POOL_SZ),
        FUSE_OPT_KEY("--allow-other",   KEY_ALLOW_OTHER),
        FUSE_OPT_KEY("-d",              KEY_DIRECT_IO),
        FUSE_OPT_KEY("--direct-io",     KEY_DIRECT_IO),
        FUSE_OPT_KEY("--no-notification", KEY_NO_LIBNOTIFY),
        FUSE_OPT_END,
};

const char * device_path = nullptr;
static struct fuse_operations ss_nullptr = { };
uint64_t buffer_pool_size = 40960;
bool allow_other = false;
bool no_libnotify = false;

static int hmnx_opt_proc(void *, const char *arg, int key,
                         struct fuse_args *outargs)
{
    switch (key)
    {
        case FUSE_OPT_KEY_NONOPT:
            if (device_path == nullptr)
            {
                device_path = strdup(arg);
                return 0;
            }

            return 1;

        case KEY_VERSION:
            printf("%s version: %s\n", PACKAGE_NAME, PACKAGE_VERSION);
            fuse_opt_add_arg(outargs, "--version");
            fuse_main(outargs->argc, outargs->argv, &ss_nullptr, nullptr);
            fuse_opt_free_args(outargs);
            exit(EXIT_SUCCESS);

        case KEY_HELP:
            usage(outargs->argv[0]);
            fuse_opt_add_arg(outargs, "-ho");
            fuse_main(outargs->argc, outargs->argv, &ss_nullptr, nullptr);
            fuse_opt_free_args(outargs);
            exit(EXIT_SUCCESS);

        case KEY_POOL_SZ:
        {
            size_t len = strlen(arg);
            size_t i = 0;

            for (; i < len; i++)
            {
                if (isdigit(arg[i]))
                {
                    break;
                }
            }

            buffer_pool_size = strtol(arg + i, nullptr, 10);
            return 0;
        }

        case KEY_ALLOW_OTHER:
            allow_other = true;
            return 0;

        case KEY_DIRECT_IO:
            filesystem_runtime_flags.filesystem_direct_io = true;
            return 0;

        case KEY_NO_LIBNOTIFY:
            no_libnotify = true;
            return 0;

        default:
            return 1;
    }
}

void init_libnotify()
{
    const char * ld_library = secure_getenv("LD_LIBRARY_PATH");
    std::vector < std::string > ld_library_list;
    uint64_t ld_lib_text_len = 0;
    std::string tmp;
    if (ld_library)
    {
        ld_lib_text_len = strlen(ld_library);
    }

    ld_library_list.emplace_back("/lib64");
    ld_library_list.emplace_back("/usr/lib64");

    for (uint64_t i = 0; i < ld_lib_text_len; i++)
    {
        if (ld_library[i] != ':')
        {
            tmp += ld_library[i];
        }
        else
        {
            ld_library_list.emplace_back(tmp);
            tmp.clear();
        }
    }

    if (!tmp.empty())
    {
        ld_library_list.emplace_back(tmp);
    }

    for (auto & path : ld_library_list)
    {
        void * handle = dlopen ((path + "/libnotify.so").c_str(), RTLD_LAZY);
        if (!handle)
        {
            continue;
        }

        libnotify_handler = handle;
        notify_init = (notify_init_t)dlsym(libnotify_handler, "notify_init");
        notify_notification_new =
                (notify_notification_new_t)dlsym(libnotify_handler, "notify_notification_new");
        notify_notification_set_timeout =
                (notify_notification_set_timeout_t)dlsym(libnotify_handler, "notify_notification_set_timeout");
        notify_notification_show =
                (notify_notification_show_t)dlsym(libnotify_handler, "notify_notification_show");

        break;
    }
}

void close_libnotify()
{
    if (libnotify_handler)
    {
        dlclose(libnotify_handler);
        libnotify_handler = nullptr;
    }
}

int main(int argc, char **argv)
{
    try
    {
        set_limit(1024 * 1024 * 64L);

        struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

        if (fuse_opt_parse(&args, nullptr, hmnx_opts, hmnx_opt_proc) == -1)
        {
            str_throw_hmnx_error("Couldn't parse the argument");
        }

        if (!device_path)
        {
            str_throw_hmnx_error("No device path provided!");
        }

        // attempting init libnotify
        if (!no_libnotify)
        {
            init_libnotify();
        }

        // init device
        device dev;
        dev.set_bpoll_sz(buffer_pool_size);
        dev.open(device_path);
        root_device = &dev;


        // setup superblock
        super_block_t _super_block { };
        auto & buffer = dev.alloc_buffer(0);
        buffer.read((char*)&_super_block, sizeof (_super_block), 0);
        buffer.drop();
        if (_super_block.magic != HMNXFS_MAGIC || _super_block.magic_comp != ~HMNXFS_MAGIC)
        {
            str_throw_hmnx_error("Unknown filesystem");
        }

        print_sb_info(_super_block);

        // setup filesystem journal
        journal _fs_journal(dev._file_info.fd,
                            _super_block.filesystem_journal_starting_block * (BLOCK_SIZE / ALIGNED_SIZE),
                            _super_block.filesystem_journal_block_count * (BLOCK_SIZE / ALIGNED_SIZE),
                            false
                            );
        fs_journal = &_fs_journal;
        root_device->add_fs_journal(fs_journal);


        // setup eblock_pool
        eblock _block_pool(dev,
                          _fs_journal,
                          _super_block.mini_bitmap_starting_block,
                          _super_block.mini_bitmap_block_count,
                          _super_block.bitmap_starting_block,
                          _super_block.mini_bitmap_block_count,
                          _super_block.snapshot_history_starting_block,
                          _super_block.snapshot_history_block_count,
                          _super_block.logic_block_starting_block,
                          _super_block.logic_block_count,
                          _super_block.logic_block_size
                );
        // TODO: Manually set block buffer size
        _block_pool.set_buffer_sz(MIN(_super_block.logic_block_count, 4096));
        block_pool = &_block_pool;


        // setup inode_pool
        inode_pool _inode_pool(dev, _block_pool, _fs_journal);
        fsinode_pool = &_inode_pool;


        /*
         * s: run single threaded
         * d: enable debugging
         * f: stay in foreground
         */
        fuse_opt_add_arg(&args, "-s");
        fuse_opt_add_arg(&args, "-d");
        fuse_opt_add_arg(&args, "-f");

        if (allow_other)
        {
            fuse_opt_add_arg(&args, "-o");
            fuse_opt_add_arg(&args, "allow_other");
            std::cout << "[FLAG] allow_other enabled" << std::endl;
            SET_BIT_OF(FILESYSTEM_FLAG_ALLOW_OTHER, fs_mount_flag);
        }

        _fs_journal.command(FILESYSTEM_MOUNT);
        _super_block.if_mount = 1;
        _super_block.last_mount_time = current_time();

        // update super block
        auto & _super_block_wr_buff = dev.alloc_buffer(0);
        _super_block_wr_buff.write((char*)&_super_block, sizeof (_super_block), 0);
        _super_block_wr_buff.drop();

        int ret = fuse_main(args.argc, args.argv, &hmnx_ops, nullptr);

        if (ret != 0)
        {
            str_throw_hmnx_error("Cannot mount due to error in FUSE!");
        }

        fuse_opt_free_args(&args);
        _fs_journal.command(FILESYSTEM_UNMOUNT);

        // show some messages
        if (notify_init)
        {
            notify_init(*argv);
            void * gnome_notification = notify_notification_new(*argv,
                                                                "[INFO]: Syncing ... do not disconnect your device!",
                                                                nullptr);
            notify_notification_set_timeout(gnome_notification, 10000); // 10 seconds
            notify_notification_show(gnome_notification, nullptr);
        }

        std::cout << "[INFO]: Syncing ... do not disconnect your device!" << std::endl;

        // close the device
        dev.close();

        std::cout << "[INFO]: Filesystem sync finished. Device can now be safely removed." << std::endl;

        if (notify_init)
        {
            void * gnome_notification = notify_notification_new(*argv,
                "[INFO]: Filesystem sync finished. Device can now be safely removed.",
                                                                nullptr);
            notify_notification_set_timeout(gnome_notification, 10000); // 10 seconds
            notify_notification_show(gnome_notification, nullptr);
        }

        close_libnotify();

        return EXIT_SUCCESS;
    }
    catch (hmnx_error_t & error)
    {
        close_libnotify();
        std::cerr << "<=== HMNX ===>\n" << error.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (std::exception & error)
    {
        close_libnotify();
        std::cerr << "<=== std::exception ===>\n" << error.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        close_libnotify();
        std::cerr << "[ERROR]: Unknown exception caught" << std::endl;
        return EXIT_FAILURE;
    }
}
