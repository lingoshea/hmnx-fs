#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <sys/types.h>
#include <path_t.h>
#include <fs.h>
#include <shs/hmnx_time.h>
#include <emulated/eblock.h>
#include <shs/sbuffer.h>
#include <shs/hmnx_error.h>
#include <iostream>
#include <shs/journal.h>
#include <shs/device.h>
#include <inode.h>
#include <fcntl.h>
#include <sys/statvfs.h>
#include <dentry.h>
#include <sys/ioctl.h>

journal *       fs_journal      = nullptr;
device *        root_device     = nullptr;
eblock *        block_pool      = nullptr;
inode_pool *    fsinode_pool    = nullptr;
/*
 * bit          meaning
 * 0            filesystem allow_other
 * 1            filesystem read_only
 * 2            filesystem disable snapshot
 * 3            filesystem disable journaling
 * 4            filesystem auto defragmentation
 * 5            bypass os cache
 * */
uint64_t        fs_mount_flag   = 0;

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MKARG(a, b) (a), (b), MIN(size, sizeof(b))

bool check_perm(mode_t mode, hmnx_inode & inode)
{
    auto context = fuse_get_context();
    uint16_t pair = mode;

    // root can just ignore the permission settings
    if (context->gid == 0 && context->uid == 0)
    {
        return true;
    }

    // consider as myself if allow_other enabled
    if (IF_FLAG_SET(FILESYSTEM_FLAG_ALLOW_OTHER, fs_mount_flag))
    {
        pair <<= 6;
        return inode.mode & pair;
    }

    if (context->gid == inode.gid)
    {
        // myself
        if (context->uid == inode.uid)
        {
            pair <<= 6;
        }
        else // same group
        {
            pair <<= 3;
        }

        return inode.mode & pair;
    }

    return inode.mode & pair;
}

uint64_t make_inode(path_t vpath, hmnx_inode & new_inode, std::string & target_name)
{
    try
    {
        if (vpath.size() != 0)
        {
            target_name = vpath[vpath.size() - 1];
            vpath.pop();
        }
        else // create root
        {
            errno = EEXIST;
            str_throw_hmnx_error("Attempt to override root inode");
        }

        auto parent = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto parent_inode = fsinode_pool->read_inode(parent);
        if (!(parent_inode.mode & S_IFDIR))
        {
            errno = ENOTDIR;
            str_throw_hmnx_error("Not a directory");
        }

        inode_filed_io inode_filed(parent_inode, *block_pool);
        dentry_t dentry{};

        strncpy(dentry.name, target_name.c_str(), sizeof (dentry.name));
        dentry.inode_logic_location = fsinode_pool->alloc();

        auto context = fuse_get_context();

        new_inode.inode_indirect_block_residence = dentry.inode_logic_location;
        new_inode.inode_direct_block_residence = dentry.inode_logic_location;
        new_inode.uid = context->uid;
        new_inode.gid = context->gid;

        hmnx::add_dentry_in_inode(inode_filed, dentry);

        // save new inode
        fsinode_pool->write_inode(new_inode, dentry.inode_logic_location);

        return dentry.inode_logic_location;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_getattr(const char *path, struct stat *stbuf)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        stbuf->st_nlink = inode.inode_links;
        stbuf->st_size  = inode.file_len;
        stbuf->st_gid   = inode.gid;
        stbuf->st_uid   = inode.uid;
        stbuf->st_mode  = inode.mode;
        stbuf->st_blocks = (inode.file_len / BLOCK_SIZE) +
                           (inode.file_len % BLOCK_SIZE == 0 ? 0 : 1);

        stbuf->st_atim  = inode.access_time;
        stbuf->st_ctim  = inode.change_time;
        stbuf->st_mtim  = inode.modify_time;

        inode.access_time = current_time();
        fsinode_pool->write_inode(inode, inode_block_num);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }

}

int hmnx_open(const char *path, struct fuse_file_info *fi)
{
    try
    {
        hmnx_access(path, PERM_READ);
        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_release(const char *, struct fuse_file_info *)
{
    try
    {
        root_device->sync();
        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}


int hmnx_read(const char *path, char *buf, size_t size,
               off_t offset, struct fuse_file_info *fi)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        inode_filed_io inode_filed(inode, *block_pool);
        return (int) inode_filed.read(buf, size, offset);
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_write(const char *path, const char *buf, size_t size,
                off_t offset, struct fuse_file_info *fi)
{
    try
    {
        if (!strcmp(path, "/.hmnx_cmd"))
        {
            return (int)size;
        }

        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        inode_filed_io inode_filed(inode, *block_pool);
        return (int)inode_filed.write(buf, size, offset);

    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                  off_t offset, struct fuse_file_info *fi)
{
    try
    {
        path_t vpath(path);
        auto inode = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto my_inode = fsinode_pool->read_inode(inode);

        inode_filed_io inode_io(my_inode, *block_pool);

        auto dentry_count = my_inode.file_len / sizeof (dentry_t);

        for (uint64_t i = 0; i < dentry_count; i++)
        {
            dentry_t dentry { };
            if (inode_io.read((char*)&dentry, sizeof (dentry), sizeof (dentry) * i)
                != sizeof (dentry))
            {
                break;
            }

            if (dentry.inode_logic_location != 0)
            {
                filler(buf, dentry.name, nullptr, 0);
            }
            else
            {
                dentry_count++;
            }
        }

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    path_t vpath(path);

    try
    {
        if (!strcmp(path, "/.hmnx_cmd"))
        {
            return 0;
        }

        auto inode_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_num);
        if (check_perm(PERM_WRTE, inode))
        {
            inode.mode = mode;
            fsinode_pool->write_inode(inode, inode_num);
            return 0;
        }

        return -EACCES;
    }
    catch (hmnx_error_t & error)
    {
        try
        {
            if (error.get_errno() == ENOENT)
            {
                std::string target_name;

                hmnx_inode new_inode = {
                        .mode = S_IFREG | mode,
                        .access_time = current_time(),
                        .change_time = current_time(),
                        .modify_time = current_time(),
                        .inode_links = 1
                };

                make_inode(vpath, new_inode, target_name);

                return 0;
            }
        } catch (...) {
            throw ;
        }

        if (error.getx_errcode() == ATTEMPT_TO_ADD_DUPLICATED_DENTRY)
        {
            return 0;
        }

        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }

}

int hmnx_link(const char *from, const char *to)
{
    try
    {
        path_t vpath_from(from), vpath_to(to);
        std::string target_name;
        if (vpath_to.size() != 0)
        {
            target_name = vpath_to[vpath_to.size() - 1];
            vpath_to.pop();
        }
        else // root
        {
            return -EEXIST;
        }

        auto from_inode_blk_num = hmnx::namei(vpath_from, *fsinode_pool, *block_pool);
        auto target_parent_inode_blk_num = hmnx::namei(vpath_to, *fsinode_pool, *block_pool);
        auto target_parent_inode = fsinode_pool->read_inode(target_parent_inode_blk_num);
        auto src_inode = fsinode_pool->read_inode(from_inode_blk_num);

        // add entry
        inode_filed_io parent_filed(target_parent_inode, *block_pool);
        dentry_t target_dentry{};

        strncpy(target_dentry.name, target_name.c_str(), sizeof(target_dentry.name));
        target_dentry.inode_logic_location = from_inode_blk_num;

        hmnx::add_dentry_in_inode(parent_filed, target_dentry);

        // update link count
        src_inode.inode_links += 1;
        fsinode_pool->write_inode(src_inode, from_inode_blk_num);

        return 0;
    }
    catch (hmnx_error_t &error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_symlink(const char *from, const char *to)
{
    try
    {
        path_t vpath_to(to);
        std::string target_name;
        hmnx_inode new_inode = {
                .mode = S_IFLNK | 0755,
                .access_time = current_time(),
                .change_time = current_time(),
                .modify_time = current_time(),
                .inode_links = 1
        };
        auto inode_number = make_inode(vpath_to, new_inode, target_name);
        new_inode = fsinode_pool->read_inode(inode_number);
        inode_filed_io new_filed(new_inode, *block_pool);
        new_filed.write(from, strlen(from), 0);

        return 0;
    }
    catch (hmnx_error_t &error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_readlink(const char *path, char *buf, size_t size)
{
    try
    {
        path_t vpath(path);
        auto inode_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_num);

        if (!S_ISLNK(inode.mode))
        {
            return -EINVAL;
        }

        inode_filed_io inode_filed(inode, *block_pool);
        inode_filed.read(buf, MIN(size - 1, inode_filed.size()), 0);
        buf[MIN(inode.file_len, size)] = 0;

        return 0;
    }
    catch (hmnx_error_t &error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_rename(const char *from, const char *to)
{
    try
    {
        if (!strcmp(from, "/.hmnx_cmd"))
        {
            return 0;
        }

        path_t vpath_from(from), vpath_to(to);
        std::string target_name, destination_name;

        // source
        if (vpath_from.size() != 0)
        {
            target_name = vpath_from[vpath_from.size() - 1];
            vpath_from.pop();
        }
        else // root
        {
            return -EEXIST;
        }

        // destination
        if (vpath_to.size() != 0)
        {
            destination_name = vpath_to[vpath_to.size() - 1];
            vpath_to.pop();
        }
        else // root
        {
            return -EEXIST;
        }

        auto from_parent_inode_blk_num = hmnx::namei(vpath_from, *fsinode_pool, *block_pool);
        auto to_parent_inode_blk_num = hmnx::namei(vpath_to, *fsinode_pool, *block_pool);
        auto from_inode = fsinode_pool->read_inode(from_parent_inode_blk_num);
        auto to_inode = fsinode_pool->read_inode(to_parent_inode_blk_num);
        inode_filed_io from_parent_filed(from_inode, *block_pool);
        inode_filed_io to_parent_filed(to_inode, *block_pool);

        auto target_inode_block_num = hmnx::remove_dentry_from_inode(from_parent_filed, target_name);
        dentry_t dentry = {
                .inode_logic_location = target_inode_block_num
        };
        strncpy(dentry.name, destination_name.c_str(), sizeof (dentry.name));
        hmnx::add_dentry_in_inode(to_parent_filed, dentry);

        return 0;
    }
    catch (hmnx_error_t &error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_mkdir(const char *path, mode_t mode)
{
    path_t vpath(path);

    try
    {
        hmnx::namei(vpath, *fsinode_pool, *block_pool);
        return -EEXIST;
    }
    catch (hmnx_error_t &error)
    {
        if (error.get_errno() == ENOENT)
        {
            try
            {
                std::string target_name;

                hmnx_inode new_inode = {
                        .mode = S_IFDIR | mode,
                        .access_time = current_time(),
                        .change_time = current_time(),
                        .modify_time = current_time(),
                        .inode_links = 2
                };

                make_inode(vpath, new_inode, target_name);

                inode_filed_io new_inode_filed(new_inode, *block_pool);

                dentry_t dentry{};

                strcpy(dentry.name, ".");
                dentry.inode_logic_location = new_inode.inode_direct_block_residence;
                new_inode_filed.write((char *) &dentry, sizeof(dentry), 0);

                strcpy(dentry.name, "..");
                dentry.inode_logic_location = new_inode.inode_direct_block_residence;
                new_inode_filed.write((char *) &dentry, sizeof(dentry), sizeof(dentry));

                return 0;
            }
            catch (hmnx_error_t &error)
            {
                std::cerr << error.what() << std::endl;
                return -errno;
            }
            catch (...)
            {
                throw ;
            }
        }
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_unlink(const char *path)
{
    try
    {
        if (!strcmp(path, "/.hmnx_cmd"))
        {
            return 0;
        }

        path_t vpath(path);

        if (vpath.size() == 0)
        {
            errno = -EPERM;
        }

        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        if (inode.inode_links == 1)
        {
            inode_filed_io inode_filed(inode, *block_pool);
            inode_filed.resize(0);
            fsinode_pool->del_inode(inode_block_num);

            std::string target_name = *(vpath.end() - 1);
            vpath.pop();
            auto parent_inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
            auto parent_inode = fsinode_pool->read_inode(parent_inode_block_num);
            inode_filed_io parent_inode_filed(parent_inode, *block_pool);
            hmnx::remove_dentry_from_inode(parent_inode_filed, target_name);
        }
        else
        {
            inode.inode_links -= 1;
            fsinode_pool->write_inode(inode, inode_block_num);
        }

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_rmdir(const char *path)
{
    try
    {
        path_t vpath(path);
        std::string target_name;
        if (vpath.size() != 0)
        {
            target_name = vpath[vpath.size() - 1];
            vpath.pop();
        }
        else // root
        {
            return -EPERM;
        }

        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        inode_filed_io inode_filed(inode, *block_pool);
        auto target_inode_num = hmnx::remove_dentry_from_inode(inode_filed, target_name);

        // deallocate all blocks
        auto target_inode = fsinode_pool->read_inode(target_inode_num);
        inode_filed_io target_filed(target_inode, *block_pool);
        if (target_filed.size() != 2 * sizeof (dentry_t))
        {
            errno = ENOTEMPTY;
            return -ENOTEMPTY;
        }

        fsinode_pool->del_inode(target_inode_num);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_utimens(const char *path, const struct timespec tv[2])
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);
        inode.access_time = tv[0];
        inode.modify_time = tv[1];
        fsinode_pool->write_inode(inode, inode_block_num);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_truncate(const char *path, off_t size)
{
    try
    {
        if (!strcmp(path, "/.hmnx_cmd"))
        {
            return 0;
        }

        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        inode_filed_io inode_filed(inode, *block_pool);
        inode_filed.resize(size);
        fsinode_pool->write_inode(inode, inode_block_num);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_chmod(const char *path, mode_t mode)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);
        inode.mode = mode;
        fsinode_pool->write_inode(inode, inode_block_num);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_chown(const char *path, uid_t uid, gid_t gid)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);
        inode.gid = gid;
        inode.uid = uid;
        fsinode_pool->write_inode(inode, inode_block_num);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_mknod(const char *path, mode_t mode, dev_t dev)
{
    try
    {
        path_t vpath(path);
        std::string target_name;
        hmnx_inode new_inode = {
                .mode = mode,
                .access_time = current_time(),
                .change_time = current_time(),
                .modify_time = current_time(),
                .inode_links = 1
        };

        new_inode.file_zone[0] = dev;

        make_inode(vpath, new_inode, target_name);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_flush (const char *, struct fuse_file_info *)
{
    try
    {
        root_device->sync();
        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_statfs (const char *, struct statvfs * _statvfs)
{
    super_block_t sb {};
    auto & super_block_buffer = root_device->alloc_buffer(0);
    super_block_buffer.read((char*)&sb, sizeof (sb), 0);
    super_block_buffer.drop();

    _statvfs->f_namemax = FS_NAME_MAX;
    _statvfs->f_flag = fs_mount_flag;

    _statvfs->f_bsize = sb.logic_block_size * BLOCK_SIZE;
    _statvfs->f_bavail = sb.logic_block_count - sb.allocated_logic_block_count;
    _statvfs->f_blocks = sb.logic_block_count;
    _statvfs->f_bfree = sb.logic_block_count - sb.allocated_logic_block_count;
    _statvfs->f_frsize = sb.logic_block_size * BLOCK_SIZE;

    _statvfs->f_files = sb.allocated_inode_count;
    _statvfs->f_ffree = _statvfs->f_blocks;
    _statvfs->f_fsid = HMNXFS_MAGIC;
    _statvfs->f_favail = _statvfs->f_ffree;

    return 0;
}

int hmnx_fsync (const char *, int, struct fuse_file_info *)
{
    root_device->sync();
    return 0;
}

// TODO: extended attributes?
int hmnx_setxattr (const char *path, const char *name, const char *value, size_t size, int flags)
{
    return -EOPNOTSUPP;
}

int hmnx_getxattr (const char *, const char *, char *, size_t)
{
    return -EOPNOTSUPP;
}

int hmnx_listxattr (const char *, char *, size_t)
{
    return -EOPNOTSUPP;
}

int hmnx_removexattr (const char *, const char *)
{
    return -EOPNOTSUPP;
}

int hmnx_fsyncdir (const char *, int, struct fuse_file_info *)
{
    root_device->sync();
    return 0;
}

int hmnx_access (const char * path, int mode)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        switch (mode)
        {
            case F_OK:
                // inode exists
                return 0;

            case R_OK:
                if (check_perm(PERM_READ, inode))
                {
                    return 0;
                }

                return -EACCES;

            case W_OK:
                if (check_perm(PERM_WRTE, inode))
                {
                    return 0;
                }

                return -EACCES;

            case X_OK:
                if (check_perm(PERM_EXEC, inode))
                {
                    return 0;
                }

                return -EACCES;

            default:
                return -EACCES;
        }
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -EPERM;
    }
}

int hmnx_lock (const char * path, struct fuse_file_info *, int cmd, struct flock * lock)
{
    try
    {
        path_t vpath(path);

        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        if (cmd == F_UNLCK)
        {
            inode.locked = INODE_UNLOCKED;
        }
        else if (cmd == F_WRLCK)
        {
            inode.locked = INODE_LOCKED_WRITE;
        }
        else if (cmd == F_RDLCK)
        {
            inode.locked = INODE_LOCKED_READ;
        }
        else
        {
            return -EINVAL;
        }

        fsinode_pool->write_inode(inode, inode_block_num);

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_ioctl (const char * path, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data)
{
    try
    {
        if (flags & FUSE_IOCTL_COMPAT)
        {
            return -ENOSYS;
        }

        path_t vpath(path);
        auto inode_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_num);

        if (!S_ISREG(inode.mode))
        {
            // TODO: redirect regular ioctl calls
            return -ENOSYS;
        }

        switch (cmd)
        {
            case HMNX_IOCTL_CREATE_SNAPSHOT:
                std::cout << "HMNX_IOCTL_CREATE_SNAPSHOT" << std::endl;
                return 0;
            case HMNX_IOCTL_DELETE_SNAPSHOT:
                std::cout << "HMNX_IOCTL_DELETE_SNAPSHOT " << *(uint64_t*)data << std::endl;
                return 0;
            case HMNX_IOCTL_DEFRAGMENTATION:
                std::cout << "HMNX_IOCTL_DEFRAGMENTATION" << std::endl;
                return 0;
            default:
                break;
        }

        return -EINVAL;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

int hmnx_fallocate (const char * path, int, off_t offset, off_t len, struct fuse_file_info *)
{
    try
    {
        path_t vpath(path);
        auto inode_block_num = hmnx::namei(vpath, *fsinode_pool, *block_pool);
        auto inode = fsinode_pool->read_inode(inode_block_num);

        if (inode.file_len < offset + len)
        {
            inode_filed_io inode_filed(inode, *block_pool);
            inode_filed.resize(offset + len);
            fsinode_pool->write_inode(inode, inode_block_num);
        }

        return 0;
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return -errno;
    }
    catch (...)
    {
        return -1;
    }
}

void* hmnx_init (struct fuse_conn_info *conn)
{
    return nullptr;
}

void hmnx_destroy   (void *)
{
}

