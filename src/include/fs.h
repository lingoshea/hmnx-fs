#ifndef HMNX_FS_FS_H
#define HMNX_FS_FS_H

#include <cstdint>
#include <path_t.h>
#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <sys/ioctl.h>

#define HMNXFS_MAGIC ((uint64_t)(0xF82142CBFF7EAD82))
#define HMNXFS_CHECKSUM_SEED HMNXFS_MAGIC

#ifndef __stable
# define __stable
#endif // __stable

struct {
    bool filesystem_direct_io = false;
} filesystem_runtime_flags;

struct super_block_t
{
    // basic information
    uint64_t magic;
    uint64_t filesystem_block_count; // in 32 kb
    uint64_t filesystem_journal_starting_block;
    uint64_t filesystem_journal_block_count;
    uint64_t mini_bitmap_starting_block;
    uint64_t mini_bitmap_block_count;
    uint64_t bitmap_starting_block;
    uint64_t bitmap_block_count;
    uint64_t snapshot_history_starting_block;
    uint64_t snapshot_history_block_count;
    uint64_t logic_block_starting_block;
    uint64_t logic_block_size;  // in 32 kb
    uint64_t logic_block_count;
    uint64_t magic_comp;

    // runtime information
    struct timespec last_mount_time;
    uint32_t if_mount:1;
    uint64_t allocated_logic_block_count;
    uint64_t allocated_inode_count;
} __attribute__((packed));


// 128 bytes
struct dentry_t
{
    char name [120];
    uint64_t inode_logic_location;
} __attribute__((packed));

#define INMAP_FIRST_LEVEL_FILED_ENTRY_COUNT 512
#define INMAP_SECND_LEVEL_FILED_ENTRY_COUNT 512
#define INMAP_THIRD_LEVEL_FILED_ENTRY_COUNT 2048

struct hmnx_inode
{
    uint32_t mode;
    struct timespec access_time;
    struct timespec change_time;
    struct timespec modify_time;
    uint64_t file_zone[INMAP_FIRST_LEVEL_FILED_ENTRY_COUNT
                       + INMAP_SECND_LEVEL_FILED_ENTRY_COUNT
                       + INMAP_THIRD_LEVEL_FILED_ENTRY_COUNT];
                                     // 512 direct (16 Mb), 512 2nd access (64 Gb), 2048 3rd access (1024 Tb)
                                     // thus we have at least 1125968643096576 mappable bytes per file
                                     // i.e. max file size is 1125968643096576 ~= 1024 Tb
    uint64_t file_len;
    uint16_t inode_links;
    uint32_t gid;
    uint32_t uid;
    uint8_t  xmode[16];
    uint64_t inode_direct_block_residence;
    uint64_t inode_indirect_block_residence;
    uint64_t locked:2;
} __attribute__((packed));

#define INODE_LOCKED_READ   0x02
#define INODE_LOCKED_WRITE  0x01
#define INODE_UNLOCKED      0x00

#define ROOT_DIR_INODE_NUM  1

class inode_pool;
class inode_filed_io;
class eblock;
class journal;
struct super_block_t;
class device;

namespace hmnx
{
    // access inode by path
    uint64_t namei(path_t, inode_pool &, eblock &);

    // remove dentry from directory inode filed
    uint64_t remove_dentry_from_inode(inode_filed_io &, const std::string &);

    // add dentry in inode
    void add_dentry_in_inode(inode_filed_io &, dentry_t &);

    // change inode information
    void eblock_count_inc(device &);

    // change inode information
    void eblock_count_dec(device &);

    // change inode information
    void inode_count_inc(device &);

    // change inode information
    void inode_count_dec(device &);
}


// FUSE APIs:
int hmnx_getattr    (const char *path, struct stat *stbuf);
int hmnx_open       (const char *path, struct fuse_file_info *fi);
int hmnx_release    (const char *path, struct fuse_file_info *fi);
int hmnx_read       (const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
int hmnx_write      (const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
int hmnx_readdir    (const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
int hmnx_create     (const char *path, mode_t mode, struct fuse_file_info *fi);
int hmnx_link       (const char *from, const char *to);
int hmnx_symlink    (const char *from, const char *to);
int hmnx_readlink   (const char *path, char *buf, size_t size);
int hmnx_rename     (const char *from, const char *to);
int hmnx_mkdir      (const char *path, mode_t mode);
int hmnx_unlink     (const char *path);
int hmnx_rmdir      (const char *path);
int hmnx_utimens    (const char *path, const struct timespec tv[2]);
int hmnx_truncate   (const char *path, off_t size);
int hmnx_chmod      (const char *path, mode_t mode);
int hmnx_chown      (const char *path, uid_t uid, gid_t gid);
int hmnx_mknod      (const char *path, mode_t mode, dev_t dev);
int hmnx_flush      (const char * path, struct fuse_file_info * fi);
int hmnx_statfs     (const char *, struct statvfs * _statvfs);
int hmnx_fsync      (const char *, int, struct fuse_file_info *);
int hmnx_setxattr   (const char *, const char *, const char *, size_t, int);
int hmnx_getxattr   (const char *, const char *, char *, size_t);
int hmnx_listxattr  (const char *, char *, size_t);
int hmnx_removexattr(const char *, const char *);
int hmnx_fsyncdir   (const char *, int, struct fuse_file_info *);
int hmnx_access     (const char *, int);
int hmnx_lock       (const char *, struct fuse_file_info *, int cmd, struct flock *);
int hmnx_ioctl      (const char *, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data);
int hmnx_fallocate  (const char *, int, off_t, off_t, struct fuse_file_info *);

void*   hmnx_init      (struct fuse_conn_info *conn);
void    hmnx_destroy   (void *);


extern journal * fs_journal;
extern device * root_device;
extern eblock * block_pool;
extern inode_pool * fsinode_pool;
extern uint64_t fs_mount_flag;

#define SET_BIT_OF(pos, tag) (tag) |= ((0x01) << (pos))
#define FILESYSTEM_FLAG_ALLOW_OTHER         0
#define FILESYSTEM_FLAG_READ_ONLY           1

#define IF_FLAG_SET(pos, tag) (((0x01) << (pos)) & (tag))

#define PERM_READ 0b100
#define PERM_WRTE 0b010
#define PERM_EXEC 0b001

#define HMNX_IOCTL_CREATE_SNAPSHOT _IOW(0xF1, 0, size_t)
#define HMNX_IOCTL_DELETE_SNAPSHOT _IOW(0xF2, 1, size_t)
#define HMNX_IOCTL_DEFRAGMENTATION _IOW(0xF3, 0, size_t)

/*
 * The following APIs are NOT tested and DO NOT promise stability
 * TODO: test these APIs and make sure they are stable
 * int hmnx_getattr    (const char *path, struct stat *stbuf);
 * int hmnx_open       (const char *path, struct fuse_file_info *fi);
 * int hmnx_release    (const char *path, struct fuse_file_info *fi);
 * int hmnx_read       (const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
 * int hmnx_write      (const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
 * int hmnx_readdir    (const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
 * int hmnx_create     (const char *path, mode_t mode, struct fuse_file_info *fi);
 * int hmnx_link       (const char *from, const char *to);
 * int hmnx_symlink    (const char *from, const char *to);
 * int hmnx_readlink   (const char *path, char *buf, size_t size);
 * int hmnx_rename     (const char *from, const char *to);
 * int hmnx_mkdir      (const char *path, mode_t mode);
 * int hmnx_unlink     (const char *path);
 * int hmnx_rmdir      (const char *path);
 * int hmnx_utimens    (const char *path, const struct timespec tv[2]);
 * int hmnx_truncate   (const char *path, off_t size);
 * int hmnx_chmod      (const char *path, mode_t mode);
 * int hmnx_chown      (const char *path, uid_t uid, gid_t gid);
 * int hmnx_mknod      (const char *path, mode_t mode, dev_t dev);
 * int hmnx_flush      (const char * path, struct fuse_file_info * fi);
 * int hmnx_statfs     (const char *, struct statvfs * _statvfs);
 * int hmnx_fsync      (const char *, int, struct fuse_file_info *);
 * int hmnx_setxattr   (const char *, const char *, const char *, size_t, int);
 * int hmnx_getxattr   (const char *, const char *, char *, size_t);
 * int hmnx_listxattr  (const char *, char *, size_t);
 * int hmnx_removexattr(const char *, const char *);
 * int hmnx_fsyncdir   (const char *, int, struct fuse_file_info *);
 * int hmnx_access     (const char *, int);
 * int hmnx_lock       (const char *, struct fuse_file_info *, int cmd, struct flock *);
 * int hmnx_bmap       (const char *, size_t blocksize, uint64_t *idx);
 * int hmnx_ioctl      (const char *, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data);
 * int hmnx_fallocate  (const char *, int, off_t, off_t, struct fuse_file_info *);
 *
 * void*   hmnx_init      (struct fuse_conn_info *conn);
 * void    hmnx_destroy   (void *);
 */
#endif //HMNX_FS_FS_H
