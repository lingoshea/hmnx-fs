#ifndef HMNX_FS_DENTRY_H
#define HMNX_FS_DENTRY_H

#include <cstdint>

#define FS_NAME_MAX (120)

struct hmnx_dentry
{
    char name [FS_NAME_MAX];
    uint64_t inode_block;
};


#endif //HMNX_FS_DENTRY_H
