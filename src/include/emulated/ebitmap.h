#ifndef HMNX_FS_EBITMAP_H
#define HMNX_FS_EBITMAP_H

#include <cstdint>

// !! Emulated block starts from 1 !! //

class device;
class eblock;
class journal;

/* block types */
#define BLOCK_UNUSED                0b00    /* This block is unused */
#define BLOCK_DATA                  0b01    /* This block is used for storage purpose */
#define BLOCK_SNAPSHOT_REDIRECT     0b10    /* This block is used for snapshot redirection */

/* block snapshot flag */
#define BLOCK_NO_SNAPSHOT_TAKEN     0b0     /* This block has no snapshot history */
#define BLOCK_SNAPSHOT_FREEZE       0b1     /* This block is frozen because of snapshot */

/* block status flag */
#define BLOCK_STATUS_NORMAL             0b00    /* This block has no status flag */
#define BLOCK_SNAPSHOT_REDIRECT_FULL    0b01    /* This block can no longer record snapshot redirect record */
#define BLOCK_SNAPSHOT_REDIRECT_APP     0b10    /* This block is a appending block for another redirect block */

#define SNAPSHOT_LOG_SIZE (64 - 1)

/* filesystem flag */
#define BLOCK_UNDEFINED         0x00    /* undefined */
#define BLOCK_INODE             0x01    /* inode */
#define BLOCK_REGULAR           0x02    /* regular (data) */
//#define BLOCK_DENTRY          0x03    /* dentry */
//#define BLOCK_REDIRPIPE       0x04    /* named pipe */
//#define BLOCK_SOCKET          0x05    /* socket */

#ifndef __stable
# define __stable
#endif // __stable

// block attribute
struct block_attribute_t
{
    uint8_t block_type:2,
            block_snapshot_flag:1,
            block_status_flag:2,
            block_fs_flag:3;
    uint8_t snapshot_log[SNAPSHOT_LOG_SIZE]; // 504
};

#define MAX_SNAPSHOT_VERSION ((SNAPSHOT_LOG_SIZE) * (8))

class ebitmap
{
protected:
    const uint64_t bitmap_starting_block;
    const uint64_t bitmap_block_count;
    const uint64_t minibitmap_starting_block;
    const uint64_t minibitmap_block_count;
    const uint64_t logic_block_count;

    device & imp_device;
    journal & fs_journal;

    uint64_t last_allocated_block_num = 1;
public:
    explicit ebitmap(device &, journal &, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t);
    block_attribute_t read_attribute(uint64_t) const;
    void write_attribute(block_attribute_t, uint64_t);

    __stable bool if_occupied(uint64_t);
    __stable void set_occupation_status(bool, uint64_t);

    uint16_t next_avail_snapshot_ver();     // return a free snapshot version AND !! UPDATE METADATA !!
    uint64_t next_avail_logic_block_num();  // return a free logic block number AND !! UPDATE METADATA !!

    friend class eblock;
};

#endif //HMNX_FS_EBITMAP_H
