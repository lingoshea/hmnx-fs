#ifndef HMNX_FS_EBLOCK_H
#define HMNX_FS_EBLOCK_H

#include <cstdint>
#include <emulated/ebitmap.h>
#include <emulated/ebuffer.h>
#include <map>
#include <unordered_map>
#include <shs/journal.h>
#include <ctime>
#include <shs/simple_bitmap.h>
#include <vector>

#define SNAPSHOT_DELETED    0
#define SNAPSHOT_TAKEN      1

#define SNAPSHOT_NO_VERSION_INDICATION (0xFFFF)

#define __stable

class device;

struct snapshot_history_t
{
    uint16_t snapshot_status:1;
    timespec timestamp;
} __attribute__((packed));

struct snapshot_redirect_t
{
    uint16_t snapshot_version;
    uint64_t logic_block;
} __attribute__((packed));

class eblock
{
protected:
    device & ops_device;

    std::unordered_map < size_t, ebuffer * > buffer_pool;
    std::vector < size_t > access_list;

    simple_bitmap buffer_bitmap;
    uint8_t * _bitmap_buffer = nullptr;
    ebuffer * _raw_buffer_pool = nullptr;
    uint64_t next_free_buffer();
    void free_buffer(uint64_t);

    // snapshot history
    void add_history(uint16_t, struct timespec);
    void delete_history(uint16_t);

    journal & fs_journal;

    uint64_t redirect_access_by_version(uint64_t, uint64_t);
public:
    const uint64_t logic_block_size;

    const uint64_t logic_block_count;
    const uint64_t logic_starting_block;

    const uint64_t history_starting_block;
    const uint64_t history_block_count;

    __stable explicit eblock(device & _dev,
                    journal & fs_journal,
                    uint64_t mini_imap_starting_block,
                    uint64_t mini_imap_size,
                    uint64_t block_imap_starting_block,
                    uint64_t block_imap_size,
                    uint64_t _history_starting_block,
                    uint64_t _history_block_count,
                    uint64_t _logic_starting_block,
                    uint64_t _logic_block_count,
                    uint64_t _logic_block_size);

    void set_buffer_sz(uint64_t);

    ebitmap block_bitmap;

    // offer a emulated logic block buffer
    // version has no effect if this logic block is not redirect block
    __stable ebuffer & access(uint64_t logic_block, uint16_t version = 0);
    // bypass block redirection
    __stable ebuffer & direct_access(uint64_t logic_block);

    ~eblock();

    // TODO: Unstable APIs:
private:
    void mkredirect(uint64_t block);
    void add_redirect_entry(uint64_t block, uint16_t version);
    void add_zero_entry(uint64_t); // add zero entry to a redirect block
    void delete_redirect_entry(uint64_t block, uint16_t version);

    uint16_t snapshot();
    void delete_snapshot(uint16_t version);
    std::map < uint16_t, struct timespec > list_history();
    struct timespec query_history(uint16_t);
    bool if_snapshoted_on(uint16_t version) const;
};

bool if_snapshoted_on(uint16_t, block_attribute_t);
void change_snapshot_status(bool, uint16_t, block_attribute_t &);

#endif //HMNX_FS_EBLOCK_H
