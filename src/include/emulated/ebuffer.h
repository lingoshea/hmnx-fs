#ifndef HMNX_FS_EBUFFER_H
#define HMNX_FS_EBUFFER_H

#include <cstddef>

class journal;
class device;
class eblock;

class ebuffer
{
protected:
    journal * fs_journal;
    device * ops_device;
    uint64_t logic_block;
    uint64_t starting_raw_block;
    uint64_t raw_block_count;
    uint64_t index;

//    explicit ebuffer(device & ops_dev,
//                     journal & _fs_journal,
//                     uint64_t _logic_block,
//                     uint64_t block_size,
//                     uint64_t _logic_starting_block);

    ebuffer() noexcept :
        logic_block(0),
        starting_raw_block(0),
        raw_block_count(0),
        fs_journal(nullptr),
        ops_device(nullptr),
        index(0)
        { }

    void be_zero();

public:
    size_t read(char * buffer, uint64_t size, uint64_t offset);
    size_t write(const char * buffer, uint64_t size, uint64_t offset);
    uint64_t my_logic_block_num() const { return logic_block + 1; }

    friend class eblock;
    ebuffer(ebuffer && copy) noexcept = delete;
    ebuffer & operator=(ebuffer && copy) = delete;
};


#endif //HMNX_FS_EBUFFER_H
