#ifndef HMNX_FS_HMNX_TIME_H
#define HMNX_FS_HMNX_TIME_H

#include <cstdint>

/*****************************************************************
 * ============================ time =============================
 * These APIs are abstractions of system specific time operation.
 *****************************************************************/

#define __stable

__stable void hmnx_usleep(unsigned int);
__stable struct timespec current_time();

#endif //HMNX_FS_HMNX_TIME_H
