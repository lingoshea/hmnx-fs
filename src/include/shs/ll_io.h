#ifndef HMNX_FS_LL_IO_H
#define HMNX_FS_LL_IO_H

/*****************************************************************
 * =================== Low-Level Input/Output ====================
 * Low-Level Input/Output (ll_io) meant to abstract system specific
 * I/O APIs. ll_io supports operation in abstracted I/O (buffered)
 * abd direct I/O (bypassing operating system buffer)
 *
 * ll_io offers operation of devices by file descriptors. OSes that
 * doesn't support file descriptor cannot use this layer.
 *****************************************************************/

#include <cstddef>

#define ALIGNED_SIZE (1024 * 32)

#define __sector
#define __file_descriptor
#define __file_name
#define __stable

typedef int fd_t;
struct file_info_t
{
    fd_t fd;
    size_t sectors;
};

__stable void ll_read(fd_t __file_descriptor, char *, size_t __sector);
__stable void ll_write(fd_t __file_descriptor, const char *, size_t __sector);
__stable file_info_t ll_open(const char * __file_name);
__stable void ll_close(fd_t __file_descriptor);

#endif //HMNX_FS_LL_IO_H
