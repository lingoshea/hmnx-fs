#ifndef HMNX_FS_SIMPLE_BITMAP_H
#define HMNX_FS_SIMPLE_BITMAP_H

#include <cstdint>
#include <sys/types.h>

#define __stable

class device;

class simple_bitmap
{
private:
    uint8_t * bits = nullptr;
    size_t count = 0;
    size_t last_allocated_free_node = 0;

public:
    __stable simple_bitmap() = default;
    __stable void init(uint8_t *, size_t);
    __stable ~simple_bitmap();
    __stable [[nodiscard]] bool get(size_t) const;
    __stable void set(size_t, bool);
    __stable size_t next_free_node();
    friend class device;
};

#endif //HMNX_FS_SIMPLE_BITMAP_H
