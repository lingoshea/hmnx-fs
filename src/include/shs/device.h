#ifndef HMNX_FS_DEVICE_H
#define HMNX_FS_DEVICE_H

/*****************************************************************
 *
 *****************************************************************/

#include <unordered_map>
#include <string>
#include <vector>

#include <shs/ll_io.h>
#include <shs/sbuffer.h>
#include <shs/simple_bitmap.h>
#include <shs/journal.h>

#define __writing_protected
#define __operation_prohibited
#define __lock_operation
#define __unlock_operation
#define __stable


__stable
class device
{
protected:
    std::unordered_map < size_t, sbuffer * > buffer_pool;
    std::vector < size_t > access_list;

    file_info_t file_info {};
    simple_bitmap buffer_bitmap;
    uint8_t * _bitmap_buffer = nullptr;
    sbuffer * _raw_buffer_pool = nullptr;

    uint64_t halt_operation:1,   // temporarily halting I/O operations
             allocating_wait:1
            ;

    journal * _fs_journal = nullptr;

    __stable __writing_protected size_t next_free_buffer(); // returns a free buffer offset
    __stable __operation_prohibited void free_buffer(size_t, bool force = false);

public:
    file_info_t & _file_info = file_info;

    __stable __lock_operation device();

    /* Always init the buffer use this function before any other operations! */
    __stable void set_bpoll_sz(size_t);
    __stable void add_fs_journal(journal * _journal) { _fs_journal = _journal; }

    __stable __unlock_operation      void open(const std::string & __file_name);
    __stable __operation_prohibited  void close();
    __stable __operation_prohibited  void sync();

    __stable __operation_prohibited  ~device();

    __stable __writing_protected sbuffer & alloc_buffer(size_t);
};


#endif //HMNX_FS_DEVICE_H
