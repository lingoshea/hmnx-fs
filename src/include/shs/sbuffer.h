#ifndef HMNX_FS_SBUFFER_H
#define HMNX_FS_SBUFFER_H

#include <cstdint>
#include <shs/ll_io.h>

#define __buffer
#define __input
#define __output
#define __length
#define __offset
#define __stable

#ifndef HMNXFS_MAGIC
# define HMNXFS_MAGIC ((uint64_t)(0xF82142CBFF7EAD82))
#endif // HMNXFS_MAGIC

#ifndef HMNXFS_CHECKSUM_SEED
# define HMNXFS_CHECKSUM_SEED HMNXFS_MAGIC
#endif // HMNXFS_CHECKSUM_SEED

typedef unsigned int dbuffer_len_t;
typedef unsigned int dbuffer_off_t;

#define BLOCK_SIZE (ALIGNED_SIZE)

class device;

__stable
class sbuffer
{
protected:
    char data [BLOCK_SIZE]{};
    uint64_t checksum = 0;
    static const dbuffer_len_t buffer_size = BLOCK_SIZE;
    uint32_t ops_dropped:1, // if read/write through this buffer is no longer possible
             ops_invoked:1,
             dirty:1,
             write_in_progress:1;
    uint64_t index = 0;
    uint64_t block_num = 0;
    fd_t file_descriptor = 0;

    __stable sbuffer() { ops_dropped = 0; ops_invoked = 0; dirty = 0; write_in_progress = 0; }
    __stable ~sbuffer() = default;

    __stable void refresh_from_disk();

public:
    __stable dbuffer_len_t read(char * __output __buffer, dbuffer_len_t __length, dbuffer_off_t __offset);
    __stable dbuffer_len_t write(const char * __input __buffer, dbuffer_len_t __length, dbuffer_off_t __offset);
    __stable void drop() { ops_dropped = 1; }
    __stable bool is_dropped() const { return ops_dropped; }

    sbuffer & operator=(const sbuffer&&) = delete;
    sbuffer(const sbuffer&&) = delete;

    friend class device;
};

__stable void set_limit(unsigned long);

#endif //HMNX_FS_SBUFFER_H
