#ifndef HMNX_FS_HMNX_ERROR_H
#define HMNX_FS_HMNX_ERROR_H

/*****************************************************************
 * =========================== error =============================
 * Class error is a public inheritance of std::exception. This
 * class is used for exception handler. This class record line
 * number, stack frames and many other useful runtime information.
 *
 * str_throw_hmnx_error(str) throws an exception with provided string
 * code_throw_hmnx_error(code) throws an exception with provided code
 *****************************************************************/

#include <exception>
#include <string>

#ifndef __stable
# define __stable
#endif // __stable

#define DULL_DESCRIPTION                                0   /* no additional description */
#define BUFFER_DEPLETED                                 1   /* buffer depleted */
#define OPERATION_HALTED                                2   /* operation halted */
#define INVALID_BUFFER                                  3   /* buffer no longer valid */
#define INVALID_INDEX                                   4   /* provided index is invalid */
#define EMPTY_BUFFER                                    5   /* buffer is empty */
#define DUPLICATED_SNAPSHOT                             6   /* trying to take the snapshot with the same ID twice */
#define SNAPSHOT_VERSION_DEPLETED                       7   /* all snapshot versions are occupied */
#define ATTEMPTING_MODIFICATION_ON_WR_PROTECTED_BUFFER  8   /* attempt modification on write protected buffer */
#define NO_SUCH_SNAPSHOT                                9   /* No such snapshot */
#define LOGIC_BLOCK_DEPLETED                            10  /* No free logic block available on filesystem */
#define NO_SUCH_LOGIC_BLOCK_WITH_PROVIDED_VERSION       11  /* Block with given snapshot version doesn't exist */
#define ALREADY_IS_REDIRECT_BLOCK                       12  /* This block is already a redirect block */
#define NOT_A_REDIRECT_BLOCK                            13  /* This block is not a redirect block */
#define INVALID_REDIRECT_ATTEMPT                        14  /* This block doesn't have corresponding snapshot version */
                                                            /* corresponding to the requested redirect */
#define ZERO_ENTRY_NOT_EMPTY                            15  /* This block already has a valid zero entry */
#define INVALID_SNAPSHOT_VERSION                        16  /* provided snapshot version is invalid */
#define NOT_AN_INODE_BLOCK                              17  /* This block is not an inode block */
#define NO_SUCH_BLOCK_BITMAP                            18  /* Invalid bitmap offset */
#define ERROR_OCCURRED_IN_FUSE                          19  /* mount attempt from fuse is failed */
#define METADATA_CORRUPTION_DETECTED                    20  /* metadata is incorrect */
#define DEALLOCATE_NON_EXISTING_LOGIC_BLOCK             21  /* attempt to delete non existing logic blocks */
#define ATTEMPT_TO_ADD_DUPLICATED_DENTRY                22  /* attempt to add duplicated dentry */


__stable
class hmnx_error_t : public std::exception
{
private:
    struct // save the machine state when the error occur
    {
        int _errno = 0;
        std::string filename;
        std::string function;
        unsigned long long line = 0;
        void * parent = nullptr;
        uint32_t additional_error_code;
    } _state_snapshot;

    std::string error;

public:
    __stable explicit hmnx_error_t(
            const std::string&,
            const std::string&,
            const std::string&,
            unsigned long long,
            void *,
            uint32_t)
            noexcept;
    __stable const char * what() const noexcept override;
    __stable int get_errno() const;
    __stable uint32_t getx_errcode() const;
};

#define str_throw_hmnx_error(str) throw hmnx_error_t("[ERROR]: " + std::string(str), __FILE__, __FUNCTION__, __LINE__, \
    __builtin_return_address(0), DULL_DESCRIPTION)
#define code_throw_hmnx_error(code) throw hmnx_error_t("[ERROR]: Internal interrupt (" + std::to_string(code) + ")", \
    __FILE__, __FUNCTION__, __LINE__, \
    __builtin_return_address(0), (code))

#endif //HMNX_FS_HMNX_ERROR_H
