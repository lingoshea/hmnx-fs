#ifndef HMNX_FS_JOURNAL_H
#define HMNX_FS_JOURNAL_H

/*****************************************************************
 * ========================= journaling ==========================
 * This file provides filesystem journaling.
 *****************************************************************/

#include <cstdint>
#include <shs/ll_io.h>
#include <cstring>

/* !! The sector size here is ALIGNED_SIZE !! */

typedef uint32_t operation_t;

#define ADD_OPERATION(ops, val) const operation_t ops = val

#define GET_L32_OF_UINT64(val) ((uint32_t)((val) & 0x00000000FFFFFFFF))
#define GET_H32_OF_UINT64(val) ((uint32_t)(((val) & 0xFFFFFFFF00000000) >> 32))
#define MK_VAL_32(val) ((uint32_t)(val))
#define MK_64bit_PARAM(val) GET_H32_OF_UINT64(val), GET_L32_OF_UINT64(val)

ADD_OPERATION(OPERATION_FINISHED,   0x0FFFFFFF);    // requested operation finished
ADD_OPERATION(_PUSH_BLOCK_NUMBER,   0x2FFFFFFE);    // _push_block_number    [block H32] [block L32]
ADD_OPERATION(_PUSH_UINT32,         0x1FFFFFFD);    // _push_block_number    [uint32]
ADD_OPERATION(_PUSH_CHECKSUM,       0x2FFFFFFC);    // _push_checksum        [checksum H32] [checksum L32]
//ADD_OPERATION(_PUSH_CUR_TIME,       0x2FFFFFFB);    // _push_cur_time        [time H32] [time L32]

ADD_OPERATION(FILESYSTEM_MOUNT,     0x00000001);    // mount
ADD_OPERATION(FILESYSTEM_UNMOUNT,   0x00000002);    // unmount
ADD_OPERATION(MKREDIRECT,           0x20000003);    // mkredirect   [block H32]     [block L32]
ADD_OPERATION(ADD_ZERO_ENTRY,       0x30000004);    // add_zero_entry   [block H32] [block L32] [version]
ADD_OPERATION(SNAPSHOT,             0x00000005);    // snapshot
ADD_OPERATION(DELETE_SNAPSHOT,      0x10000006);    // delete_snapshot   [version]
ADD_OPERATION(MODIFY_BLOCK_ATTR,    0x20000007);    // modify_block_attr [block H32] [block L32]
ADD_OPERATION(MODIFY_BLOCK_CONT,    0x20000008);    // modify_block_cont [block H32] [block L32]
ADD_OPERATION(FILESYSTEM_ERROR,     0x10000009);    // filesystem_error  [error code]
ADD_OPERATION(ADD_SNAPSHOT_HISTORY, 0x1000000A);    // add_snapshot_history  [version]
ADD_OPERATION(DEL_SNAPSHOT_HISTORY, 0x1000000B);    // del_snapshot_history  [version]
ADD_OPERATION(ADD_REDIRECT_ENTRY,   0x3000000C);    // add_redirect_entry    [block H32] [block L32] [version]
ADD_OPERATION(DEL_REDIRECT_ENTRY,   0x3000000D);    // del_redirect_entry    [block H32] [block L32] [version]
ADD_OPERATION(ALLOCATE_INODE,       0x0000000E);    // allocate_inode
ADD_OPERATION(DEALLOCATE_INODE,     0x2000000F);    // deallocate_inode      [block H32] [block L32]
ADD_OPERATION(ADD_INODE_BLOCK_ENTRY,0x40000010);    // add_inode_block_entry [block H32] [block L32] [block H32] [block L32]
ADD_OPERATION(DEL_INODE_BLOCK_ENTRY,0x40000011);    // del_inode_block_entry [block H32] [block L32] [block H32] [block L32]
ADD_OPERATION(MODIFY_INODE_CONT,    0x20000012);    // modify_inode_cont     [block H32] [block L32]
ADD_OPERATION(MODIFY_OCCUPATION_STATUS,0x20000013); // modify_occupation_status     [block H32] [block L32]
ADD_OPERATION(CHECKSUM_IN_BLOCK,    0x40000014);    // checksum_in_block     [block H32] [block L32] [checksum H32] [checksum L32]
ADD_OPERATION(FILESYSTEM_SYNC,      0x00000015);    // filesystem_sync

#ifndef __stable
# define __stable
#endif // __stable

class journal
{
private:
    int fd;
    uint64_t journal_sector_size;
    uint64_t journal_starting_sector;
    uint64_t max_journal_entry;
    uint64_t buffer_size;

    static const uint32_t BUFFER_BLOCK_SIZE = ALIGNED_SIZE;

    static const uint32_t DOFF_READ_OFFSET = 1;
    static const uint32_t DOFF_WRITE_OFFSET = 2;
    static const uint32_t DOFF_NULL_POINT_OFFSET = 3;

    static const uint32_t NULL_POINT_ACTIVE = 1;
    static const uint32_t NULL_POINT_INACTIVE = 0;

    static const uint32_t START_OFFSET = 3;

protected:
    bool ignore_r_off = false;
    bool inited_native_r_off = false;
    uint32_t native_r_off = 0;
    bool native_n_point = false;

    __stable uint32_t read_number(uint64_t offset) const;
    __stable void update_number(uint32_t val, uint64_t offset);

    __stable uint32_t read_offset();
    __stable void update_r_offset(uint32_t);

    __stable uint32_t write_offset();
    __stable void update_w_offset(uint32_t);

    __stable uint32_t null_point();
    __stable void update_n_point(uint32_t);

public:
    __stable explicit journal(fd_t, uint64_t, uint64_t, bool recovery = false);
    __stable void push(uint32_t operation);
    __stable uint32_t pop(bool erase);

    __stable void clear();

    __stable void command(uint32_t cmd, ...);
};


#endif //HMNX_FS_JOURNAL_H
