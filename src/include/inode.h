#ifndef HMNX_FS_INODE_H
#define HMNX_FS_INODE_H

#include <cstdint>

#include <emulated/eblock.h>
#include <shs/device.h>
#include <fs.h>

class inode_pool
{
private:
    device & filesystem;
    journal & fs_journal;
    eblock & eblock_pool;

public:
    explicit
    inode_pool(device & _filesystem,
               eblock & _eblock_pool,
               journal & _fs_journal
               )
               :
                filesystem(_filesystem),
                eblock_pool(_eblock_pool),
                fs_journal(_fs_journal)
    {}

    uint64_t alloc(); // allocate an inode, return inode number
    void del_inode(uint64_t block_num);
    hmnx_inode read_inode(uint64_t block_num);
    void write_inode(hmnx_inode& inode, uint64_t block_num);
};

class inode_filed_io
{
private:
    hmnx_inode & my_inode;
    eblock & eblock_pool;

    void refresh_inode();

    uint64_t inode_query_logic_block_entry(uint64_t);
    void alloc_at_first_level(uint64_t);
    void dealloc_at_first_level(uint64_t);

    void alloc_at_second_level(uint64_t);
    void dealloc_at_second_level(uint64_t);

    void alloc_at_third_level(uint64_t);
    void dealloc_at_third_level(uint64_t);

    void alloc_in_blk_entry(uint64_t, uint64_t);
    void dealloc_in_blk_entry(uint64_t, uint64_t);

    void add_block();
    void del_block();

    uint64_t simple_alloc_blk();
    void simple_dealloc_blk(uint64_t);

    uint64_t get_block_num_by_off(uint64_t, uint64_t);
    void write_block_num_by_off(uint64_t, uint64_t, uint64_t);
    void dealloc_in_block(uint64_t, uint64_t, uint64_t);
    void alloc_in_block(uint64_t, uint64_t, uint64_t);

    void update_inode();

public:
    explicit inode_filed_io(hmnx_inode & _inode, eblock & _eblock_pool)
    : my_inode(_inode), eblock_pool(_eblock_pool) { }

    size_t read(char *, uint64_t, uint64_t);
    size_t write(const char *, uint64_t, uint64_t, bool require_resize = true);
    void resize(size_t);
    uint64_t size() { refresh_inode(); return my_inode.file_len; }
};

#endif //HMNX_FS_INODE_H
