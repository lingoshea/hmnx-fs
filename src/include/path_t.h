#ifndef HMNX_FS_PATH_T_H
#define HMNX_FS_PATH_T_H

#include <vector>
#include <string>

#define __stable


typedef std::vector < std::string >::iterator path_iterator_t;

// This class auto translates FUSE path to vector path
__stable
class path_t
{
protected:
    std::vector < std::string > _path;

public:
    __stable const std::vector < std::string > & path = _path;
    __stable std::string operator [](unsigned int);
    __stable explicit path_t(std::string fuse_path);
    __stable path_iterator_t begin() { return _path.begin(); }
    __stable path_iterator_t end() { return _path.end(); }
    __stable void pop() { _path.pop_back(); }
    __stable size_t size() const { return _path.size(); }
};

#endif //HMNX_FS_PATH_T_H
