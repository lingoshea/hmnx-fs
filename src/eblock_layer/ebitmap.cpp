#include <emulated/ebitmap.h>
#include <shs/device.h>
#include <shs/hmnx_error.h>
#include <shs/journal.h>
#include <fs.h>

ebitmap::ebitmap(device & dev,
                 journal & _fs_journal,
                 uint64_t _minibitmap_starting_block,
                 uint64_t _minibitmap_block_count,
                 uint64_t _bitmap_starting_block,
                 uint64_t _bitmap_block_count,
                 uint64_t _logic_block_count
                 )
                 :  bitmap_starting_block(_bitmap_starting_block),
                    bitmap_block_count(_bitmap_block_count),
                    minibitmap_starting_block(_minibitmap_starting_block),
                    minibitmap_block_count(_minibitmap_block_count),
                    logic_block_count(_logic_block_count),
                    imp_device(dev),
                    fs_journal(_fs_journal)
{
}

// read block attributes
block_attribute_t ebitmap::read_attribute(uint64_t offset) const
{
    if (offset > logic_block_count)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NO_SUCH_BLOCK_BITMAP));
        code_throw_hmnx_error(NO_SUCH_BLOCK_BITMAP);
    }

    block_attribute_t ret{};
    auto block = sizeof (block_attribute_t) * offset / BLOCK_SIZE;
    auto block_offset = (sizeof (block_attribute_t) * offset) % BLOCK_SIZE;
    auto & buffer = imp_device.alloc_buffer(block + bitmap_starting_block);
    buffer.read((char *) &ret, sizeof(ret), block_offset);
    buffer.drop();
    return ret;
}

// write block attributes
void ebitmap::write_attribute(block_attribute_t attr, uint64_t offset)
{
    fs_journal.command(MODIFY_BLOCK_ATTR, GET_H32_OF_UINT64(offset), GET_L32_OF_UINT64(offset));

    if (offset > logic_block_count)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NO_SUCH_BLOCK_BITMAP));
        code_throw_hmnx_error(NO_SUCH_BLOCK_BITMAP);
    }

    auto block = sizeof (block_attribute_t) * offset / BLOCK_SIZE;
    auto block_offset = (sizeof (block_attribute_t) * offset) % BLOCK_SIZE;
    auto & buffer = imp_device.alloc_buffer(block + bitmap_starting_block);
    buffer.write((char*)&attr, sizeof (attr), block_offset);
    buffer.drop();

    fs_journal.command(OPERATION_FINISHED);
}

// get next available snapshot version
uint16_t ebitmap::next_avail_snapshot_ver()
{
    auto attr = read_attribute(0);

    for (uint64_t i = 0; i < sizeof (attr.snapshot_log); i++)
    {
        uint8_t bit = 0x01;
        for (uint64_t off = 0; off < 8; off++, bit <<= 1)
        {
            if (!(attr.snapshot_log[i] & bit))
            {
                attr.snapshot_log[i] |= bit;
                auto id = i * 8 + off + 1;
                write_attribute(attr, 0);

                return id;
            }

        }
    }

    fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(SNAPSHOT_VERSION_DEPLETED));
    code_throw_hmnx_error(SNAPSHOT_VERSION_DEPLETED);
}

// get next available logic block number
uint64_t ebitmap::next_avail_logic_block_num()
{
    uint64_t i;
    bool redo = false;

    start:
    if (last_allocated_block_num == logic_block_count)
    {
        last_allocated_block_num = 1;
    }

    for (i = last_allocated_block_num; i <= logic_block_count; i++)
    {
        if (!if_occupied(i))
        {
            last_allocated_block_num = i;
            return i;
        }
    }

    if (!redo)
    {
        redo = true;
        goto start;
    }

    errno = ENOSPC;
    fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(LOGIC_BLOCK_DEPLETED));
    code_throw_hmnx_error(LOGIC_BLOCK_DEPLETED);
}

// if one specific block is occupied in bitmap
bool ebitmap::if_occupied(uint64_t offset)
{
    offset -= 1;
    if (offset > logic_block_count)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NO_SUCH_BLOCK_BITMAP));
        code_throw_hmnx_error(NO_SUCH_BLOCK_BITMAP);
    }

    auto block = (offset / 8) / BLOCK_SIZE;
    auto block_offset = (offset / 8) % BLOCK_SIZE;
    auto bit_place = offset % 8;
    uint8_t bit_buffer, dummy = 0x01 << bit_place;
    auto & buffer = imp_device.alloc_buffer(block + minibitmap_starting_block);
    buffer.read((char*)&bit_buffer, sizeof (bit_buffer), block_offset);
    buffer.drop();

    return bit_buffer & dummy;
}

// set occupation status in bitmap
void ebitmap::set_occupation_status(bool status, uint64_t offset)
{
    fs_journal.command(MODIFY_OCCUPATION_STATUS, GET_H32_OF_UINT64(offset), GET_L32_OF_UINT64(offset));

    offset -= 1;
    if (offset > logic_block_count)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NO_SUCH_BLOCK_BITMAP));
        code_throw_hmnx_error(NO_SUCH_BLOCK_BITMAP);
    }

    auto block = (offset / 8) / BLOCK_SIZE;
    auto block_offset = (offset / 8) % BLOCK_SIZE;
    auto bit_place = offset % 8;
    uint8_t bit_buffer, dummy = 0x01 << bit_place;
    auto & buffer = imp_device.alloc_buffer(block + minibitmap_starting_block);
    buffer.read((char*)&bit_buffer, sizeof (bit_buffer), block_offset);
    bit_buffer &= ~dummy; // delete that bit
    if (status)
    {
        bit_buffer |= dummy; // add in if status is true
    }
    buffer.write((char*)&bit_buffer, sizeof (bit_buffer), block_offset);
    buffer.drop();

    if (status)
    {
        hmnx::eblock_count_inc(imp_device);
    }
    else
    {
        hmnx::eblock_count_dec(imp_device);
    }

    fs_journal.command(OPERATION_FINISHED);
}
