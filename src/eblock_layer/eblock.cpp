#include <emulated/eblock.h>
#include <shs/device.h>
#include <shs/hmnx_error.h>
#include <shs/hmnx_time.h>
#include <cstring>
#include <ctime>

eblock::eblock(device &_dev,
               journal & _fs_journal,
               uint64_t mini_imap_starting_block,
               uint64_t mini_imap_size,
               uint64_t block_imap_starting_block,
               uint64_t block_imap_size,
               uint64_t _history_starting_block,
               uint64_t _history_block_count,
               uint64_t _logic_starting_block,
               uint64_t _logic_block_count,
               uint64_t _logic_block_size)
               :
               ops_device(_dev),
               history_starting_block(_history_starting_block),
               history_block_count(_history_block_count),
               logic_starting_block(_logic_starting_block),
               logic_block_count(_logic_block_count),
               logic_block_size(_logic_block_size),
               fs_journal(_fs_journal),
               block_bitmap(_dev, fs_journal,
                            mini_imap_starting_block,
                            mini_imap_size,
                            block_imap_starting_block,
                            block_imap_size,
                            _logic_block_count
                            )

{
}

ebuffer & eblock::access(uint64_t logic_block, uint16_t version)
{
    auto attr = block_bitmap.read_attribute(logic_block);
    if (attr.block_type == BLOCK_SNAPSHOT_REDIRECT)
    {
        direct_access(redirect_access_by_version(logic_block, version));
    }

    return direct_access(logic_block);
}

uint16_t eblock::snapshot()
{
    fs_journal.command(SNAPSHOT);

    uint64_t i;
    uint16_t version = block_bitmap.next_avail_snapshot_ver();

    fs_journal.command(_PUSH_UINT32, MK_VAL_32(version));

    auto cur_time = current_time();
    add_history(version, cur_time);

    // snapshot dummy
    auto _0_attr = block_bitmap.read_attribute(0);
    change_snapshot_status(true, version, _0_attr);
    block_bitmap.write_attribute(_0_attr, 0);

    for (i = 1; i < logic_block_count; i++)
    {
        if (block_bitmap.if_occupied(i))
        {
            auto attr = block_bitmap.read_attribute(i);
            change_snapshot_status(true, version, attr);
            attr.block_snapshot_flag = BLOCK_SNAPSHOT_FREEZE;
            block_bitmap.write_attribute(attr, i);
        }
    }

    fs_journal.command(OPERATION_FINISHED);

    return version;
}

void eblock::add_history(uint16_t version, struct timespec time)
{
    fs_journal.command(ADD_SNAPSHOT_HISTORY,
                       MK_VAL_32(version));

    uint64_t block = (version * sizeof (snapshot_history_t)) / (logic_block_size * BLOCK_SIZE);
    uint64_t block_offset = (version * sizeof (snapshot_history_t)) % (logic_block_size * BLOCK_SIZE);
    snapshot_history_t history = {
            .snapshot_status = SNAPSHOT_TAKEN,
            .timestamp = time,
    };
    auto & buffer = ops_device.alloc_buffer(block + history_starting_block);
    buffer.write((char*)&history, sizeof (history), block_offset);
    buffer.drop();

    fs_journal.command(OPERATION_FINISHED);
}

struct timespec eblock::query_history(uint16_t version)
{
    uint64_t block = (version * sizeof (snapshot_history_t)) / (logic_block_size * BLOCK_SIZE);
    uint64_t block_offset = (version * sizeof (snapshot_history_t)) % (logic_block_size * BLOCK_SIZE);
    snapshot_history_t history{};
    auto & buffer = ops_device.alloc_buffer(block + history_starting_block);
    buffer.read((char*)&history, sizeof (history), block_offset);
    buffer.drop();
    return history.timestamp;
}

void eblock::delete_history(uint16_t version)
{
    fs_journal.command(DEL_SNAPSHOT_HISTORY, (uint32_t)version);

    uint64_t block = (version * sizeof (snapshot_history_t)) / (logic_block_size * BLOCK_SIZE);
    uint64_t block_offset = (version * sizeof (snapshot_history_t)) % (logic_block_size * BLOCK_SIZE);
    snapshot_history_t history = {
            .snapshot_status = SNAPSHOT_DELETED,
            .timestamp{},
    };
    auto & buffer = ops_device.alloc_buffer(block + history_starting_block);
    buffer.write((char*)&history, sizeof (history), block_offset);
    buffer.drop();

    fs_journal.command(OPERATION_FINISHED);
}

std::map < uint16_t, struct timespec > eblock::list_history()
{
    std::map < uint16_t, struct timespec > list;
    auto attr = block_bitmap.read_attribute(0);

    for (uint16_t version = 1; version <= MAX_SNAPSHOT_VERSION; version++)
    {
        uint64_t block = (version * sizeof(snapshot_history_t)) / (logic_block_size * BLOCK_SIZE);
        uint64_t block_offset = (version * sizeof(snapshot_history_t)) % (logic_block_size * BLOCK_SIZE);
        snapshot_history_t history{};
        auto &buffer = ops_device.alloc_buffer(block + history_starting_block);
        buffer.read((char *) &history, sizeof(history), block_offset);

        if (history.snapshot_status != SNAPSHOT_DELETED)
        {
            if (::if_snapshoted_on(version, attr))
            {
                list.insert(std::make_pair(version, (timespec)history.timestamp));
            }
            else // auto correct
            {
                history.snapshot_status = SNAPSHOT_DELETED;
                buffer.write((char *) &history, sizeof(history), block_offset);
            }
        }

        buffer.drop();
    }

    return list;
}

void eblock::delete_snapshot(uint16_t version)
{
    fs_journal.command(DELETE_SNAPSHOT, MK_VAL_32(version));

    if (!if_snapshoted_on(version))
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NO_SUCH_SNAPSHOT));
        code_throw_hmnx_error(NO_SUCH_SNAPSHOT);
    }

    auto _0_attr = block_bitmap.read_attribute(0);
    change_snapshot_status(false, version, _0_attr);
    block_bitmap.write_attribute(_0_attr, 0);

    for (uint64_t i = 1; i < logic_block_count; i++)
    {
        if (block_bitmap.if_occupied(i))
        {
            auto attr = block_bitmap.read_attribute(i);
            if (::if_snapshoted_on(version, attr))
            {
                if (attr.block_type == BLOCK_SNAPSHOT_REDIRECT
                    && attr.block_status_flag != BLOCK_SNAPSHOT_REDIRECT_APP)
                {
                    delete_redirect_entry(i, version);
                }

                change_snapshot_status(false, version, attr);
                char null_log[SNAPSHOT_LOG_SIZE]{};
                // if this is a stray block
                if (!memcmp(attr.snapshot_log, null_log, sizeof(null_log)))
                {
                    block_bitmap.set_occupation_status(false, i);
                    attr.block_type = BLOCK_UNUSED;
                }

                block_bitmap.write_attribute(attr, i);
            }
        }
    }

    delete_history(version);

    fs_journal.command(OPERATION_FINISHED);
}

bool eblock::if_snapshoted_on(uint16_t version) const
{
    return ::if_snapshoted_on(version, block_bitmap.read_attribute(0));
}

void eblock::mkredirect(uint64_t block)
{
    fs_journal.command(MKREDIRECT, GET_H32_OF_UINT64(block), GET_L32_OF_UINT64(block));

    if (block_bitmap.read_attribute(block).block_type == BLOCK_SNAPSHOT_REDIRECT)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(ALREADY_IS_REDIRECT_BLOCK));
        code_throw_hmnx_error(ALREADY_IS_REDIRECT_BLOCK);
    }

    auto & old_block_buffer = direct_access(block);
    auto new_block = block_bitmap.next_avail_logic_block_num();
    auto & new_block_buffer = direct_access(new_block);

    // copy attribute
    auto old_attribute = block_bitmap.read_attribute(block);
    block_bitmap.write_attribute(old_attribute, new_block);
    // mark used
    block_bitmap.set_occupation_status(true, new_block);

    // copy content
    char * tmp_buffer = new char [logic_block_size * BLOCK_SIZE];
    old_block_buffer.read(tmp_buffer, logic_block_size * BLOCK_SIZE, 0);
    new_block_buffer.write(tmp_buffer, logic_block_size * BLOCK_SIZE, 0);
    delete []tmp_buffer;

    // modify old block attribute
    old_attribute.block_type = BLOCK_SNAPSHOT_REDIRECT;
    block_bitmap.write_attribute(old_attribute, block);

    uint64_t redirect_count = 1;
    snapshot_redirect_t zero_redirect { };
    old_block_buffer.write((char*)&zero_redirect, sizeof (zero_redirect), sizeof (redirect_count));

    // add all the snapshot version
    snapshot_redirect_t redirect { };
    for (uint16_t version = 1; version <= MAX_SNAPSHOT_VERSION; version++)
    {
        if (::if_snapshoted_on(version, old_attribute))
        {
            redirect.snapshot_version = version;
            redirect.logic_block = new_block;

            old_block_buffer.write((char*)&redirect, sizeof (redirect),
                                   version * sizeof (redirect) + sizeof (redirect_count));
            redirect_count++;
        }
    }

    // write redirect count
    old_block_buffer.write((char*)&redirect_count, sizeof (redirect_count), 0);

    fs_journal.command(OPERATION_FINISHED);
}

void eblock::add_redirect_entry(uint64_t block, uint16_t version)
{
    fs_journal.command(ADD_REDIRECT_ENTRY,
                       GET_H32_OF_UINT64(block),
                       GET_L32_OF_UINT64(block),
                       MK_VAL_32(version));

    if (block_bitmap.read_attribute(block).block_type != BLOCK_SNAPSHOT_REDIRECT)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NOT_A_REDIRECT_BLOCK));
        code_throw_hmnx_error(NOT_A_REDIRECT_BLOCK);
    }

    if (!::if_snapshoted_on(version, block_bitmap.read_attribute(block)))
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(INVALID_REDIRECT_ATTEMPT));
        code_throw_hmnx_error(INVALID_REDIRECT_ATTEMPT);
    }

    auto & current_redirect_block = direct_access(block);

    snapshot_redirect_t redirect {};
    uint64_t zero_logic_block = 0;
    uint64_t redirect_count = 0;

    // get redirect count
    current_redirect_block.read((char*)&redirect_count, sizeof (redirect_count), 0);

    // get first entry
    current_redirect_block.read((char*)&redirect, sizeof (redirect), sizeof (uint64_t));
    zero_logic_block = redirect.logic_block;

    memset(&redirect, 0, sizeof (redirect));
    // clear first entry
    current_redirect_block.write((char*)&redirect, sizeof (redirect), sizeof (uint64_t));

    // add entry
    if (sizeof (uint64_t) + redirect_count * sizeof(redirect) < logic_block_size * BLOCK_SIZE)
    {
        redirect.logic_block = zero_logic_block;
        redirect.snapshot_version = version;
        current_redirect_block.write((char*)&redirect, sizeof (redirect),
                                 sizeof (uint64_t) + redirect_count * sizeof(redirect));

        // update redirect_count
        redirect_count += 1;
        current_redirect_block.write((char*)&redirect_count, sizeof (redirect_count), 0);
    }
    else
    {
        while (sizeof (uint64_t) + redirect_count * sizeof(redirect) > logic_block_size * BLOCK_SIZE)
        {
            auto & redirect_block_chain = direct_access(block);
            redirect_block_chain.read((char*)&redirect_count, sizeof (redirect_count), 0);

            if (sizeof (uint64_t) + redirect_count * sizeof(redirect) == logic_block_size * BLOCK_SIZE)
            {
                // -- setup new redirect block --
                auto new_redirect_block = block_bitmap.next_avail_logic_block_num();
                auto & nr_blk_buffer = direct_access(new_redirect_block);
                // copy attributes
                auto cur_redirect_blk_attr = block_bitmap.read_attribute(block);
                cur_redirect_blk_attr.block_status_flag = BLOCK_SNAPSHOT_REDIRECT_APP;
                block_bitmap.write_attribute(cur_redirect_blk_attr, new_redirect_block);
                block_bitmap.set_occupation_status(true, new_redirect_block);
                // read last redirect info
                redirect_block_chain.read((char*)&redirect, sizeof (redirect),
                                               sizeof (uint64_t) + (redirect_count - 1) * sizeof(redirect));

                // add entry to new redirect block
                uint64_t new_redirect_count = 2;
                nr_blk_buffer.write((char*)&new_redirect_count, sizeof (new_redirect_count), 0);
                nr_blk_buffer.write((char*)&redirect, sizeof (redirect), sizeof (new_redirect_block));

                // replace last entry
                redirect.snapshot_version = SNAPSHOT_NO_VERSION_INDICATION;
                redirect.logic_block = new_redirect_block;
                redirect_block_chain.write((char*)&redirect, sizeof (redirect),
                                                sizeof (uint64_t) + (redirect_count - 1) * sizeof(redirect));

                // mark redirect block full
                cur_redirect_blk_attr.block_status_flag |= BLOCK_SNAPSHOT_REDIRECT_FULL;
                block_bitmap.write_attribute(cur_redirect_blk_attr, block);

                block = new_redirect_block;
                break;
            }

            redirect_block_chain.read((char*)&redirect, sizeof (redirect),
                                       sizeof (uint64_t) + (redirect_count - 1) * sizeof(redirect));
            block = redirect.logic_block;
        }

        auto & last_avail_redirect_block = direct_access(block);
        auto last_avail_redirect_block_attr = block_bitmap.read_attribute(block);
        last_avail_redirect_block.read((char*)&redirect_count, sizeof (redirect_count), 0);

        // add new entry in
        redirect.snapshot_version = version;
        redirect.logic_block = zero_logic_block;
        last_avail_redirect_block.write((char*)&redirect, sizeof (redirect),
                            sizeof (uint64_t) + sizeof (redirect) * redirect_count);

        // update new redirect_count
        redirect_count += 1;
        last_avail_redirect_block.write((char*)&redirect_count, sizeof (redirect_count), 0);
    }

    fs_journal.command(OPERATION_FINISHED);
}

ebuffer & eblock::direct_access(uint64_t logic_block)
{
    auto it = buffer_pool.find(logic_block);

    if (it != buffer_pool.end())
    {
        return *it->second;
    }

    auto index = next_free_buffer();
    auto & buffer = _raw_buffer_pool[index];
    buffer.ops_device = &ops_device;
    buffer.fs_journal = &fs_journal;
    buffer.logic_block = logic_block - 1;
    buffer.raw_block_count = logic_block_size;
    buffer.starting_raw_block = buffer.logic_block * logic_block_size + logic_starting_block;

    buffer_bitmap.set(index, true);
    access_list.emplace_back(logic_block);
    buffer_pool.emplace(logic_block, &buffer);
    return buffer;
}

void eblock::add_zero_entry(uint64_t block)
{
    fs_journal.command(ADD_ZERO_ENTRY, GET_H32_OF_UINT64(block), GET_L32_OF_UINT64(block));

    if (block_bitmap.read_attribute(block).block_type != BLOCK_SNAPSHOT_REDIRECT)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NOT_A_REDIRECT_BLOCK));
        code_throw_hmnx_error(NOT_A_REDIRECT_BLOCK);
    }

    // check if zero entry is empty
    auto & current_redirect_block = direct_access(block);
    snapshot_redirect_t redirect { };
    current_redirect_block.read((char*)&redirect, sizeof (redirect), sizeof (uint64_t));
    if (redirect.logic_block != 0)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(ZERO_ENTRY_NOT_EMPTY));
        code_throw_hmnx_error(ZERO_ENTRY_NOT_EMPTY);
    }

    // setup new data block
    auto new_block = block_bitmap.next_avail_logic_block_num();
    block_attribute_t attr = { .block_type = BLOCK_DATA };
    block_bitmap.write_attribute(attr, new_block);
    block_bitmap.set_occupation_status(true, new_block);

    // refresh zero entry
    redirect.logic_block = new_block;
    current_redirect_block.write((char*)&redirect, sizeof (redirect), sizeof (uint64_t));

    fs_journal.command(OPERATION_FINISHED);
}

void eblock::delete_redirect_entry(uint64_t block, uint16_t version)
{
    fs_journal.command(DEL_REDIRECT_ENTRY,
                       GET_H32_OF_UINT64(block),
                       GET_L32_OF_UINT64(block),
                       MK_VAL_32(version));

    if (block_bitmap.read_attribute(block).block_type != BLOCK_SNAPSHOT_REDIRECT)
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NOT_A_REDIRECT_BLOCK));
        code_throw_hmnx_error(NOT_A_REDIRECT_BLOCK);
    }

    if (!::if_snapshoted_on(version, block_bitmap.read_attribute(block)))
    {
        fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(INVALID_REDIRECT_ATTEMPT));
        code_throw_hmnx_error(INVALID_REDIRECT_ATTEMPT);
    }

    uint64_t redirect_count = 0;
    uint16_t read_version = 0;
    int64_t offset = 0;
    uint64_t query_block = block;
    auto & _buffer = direct_access(query_block);
    _buffer.read((char*)&redirect_count, sizeof (redirect_count), 0);

    snapshot_redirect_t redirect{};
    for (uint64_t i = 0 ; i < redirect_count ; i++)
    {
        auto & buffer = direct_access(query_block);
        if (buffer.read((char *) &redirect,
                        sizeof(redirect),
                        offset * sizeof(redirect) + sizeof (uint64_t))
            == sizeof(redirect))
        {
            // if indication is that there is another redirect block after this
            if (redirect.snapshot_version == SNAPSHOT_NO_VERSION_INDICATION)
            {
                // reset loop
                query_block = redirect.logic_block;

                // refresh redirect_count
                auto & new_redirect_blk_buffer = direct_access(query_block);

                new_redirect_blk_buffer.read((char*)&redirect_count,
                                             sizeof (redirect_count),
                                             0);

                i = 0;
                offset = 0;

                continue;
            }

            read_version = redirect.snapshot_version;

            if (read_version == version)
            {
                memset(&redirect, 0, sizeof (redirect));
                buffer.write((char *) &redirect,
                            sizeof(redirect),
                            offset * sizeof(redirect) + sizeof (uint64_t));
                goto end;
            }

            offset++;
            continue;
        }
    }

    fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NO_SUCH_LOGIC_BLOCK_WITH_PROVIDED_VERSION));
    code_throw_hmnx_error(NO_SUCH_LOGIC_BLOCK_WITH_PROVIDED_VERSION);

    end:
    fs_journal.command(OPERATION_FINISHED);
}

uint64_t eblock::redirect_access_by_version(uint64_t logic_block, uint64_t version)
{
    uint64_t redirect_count = 0;
    uint16_t read_version = 0;
    int64_t offset = 0;
    uint64_t actual_logic_block = 0;
    uint64_t query_block = logic_block;
    auto & _buffer = direct_access(query_block);
    _buffer.read((char*)&redirect_count, sizeof (redirect_count), 0);

    snapshot_redirect_t redirect{};
    for (uint64_t i = 0 ; i < redirect_count ; i++)
    {
        auto & buffer = direct_access(query_block);
        if (buffer.read((char *) &redirect,
                        sizeof(redirect),
                        offset * sizeof(redirect) + sizeof (uint64_t))
            == sizeof(redirect))
        {
            // if indication is that there is another redirect block after this
            if (redirect.snapshot_version == SNAPSHOT_NO_VERSION_INDICATION)
            {
                // reset loop
                query_block = redirect.logic_block;

                // refresh redirect_count
                auto & new_redirect_blk_buffer = direct_access(query_block);
                new_redirect_blk_buffer.read((char*)&redirect_count,
                                             sizeof (redirect_count),
                                             0);

                i = 0;
                offset = 0;

                continue;
            }

            read_version = redirect.snapshot_version;
            actual_logic_block = redirect.logic_block;

            if (read_version == version)
            {
                goto end;
            }

            offset++;
            continue;
        }
    }

    fs_journal.command(FILESYSTEM_ERROR, MK_VAL_32(NO_SUCH_LOGIC_BLOCK_WITH_PROVIDED_VERSION));
    code_throw_hmnx_error(NO_SUCH_LOGIC_BLOCK_WITH_PROVIDED_VERSION);

    end:
    return actual_logic_block;
}

void eblock::set_buffer_sz(uint64_t pool_size)
{
    try
    {
        if (!pool_size)
        {
            str_throw_hmnx_error("Cache space cannot be 0");
        }

        _raw_buffer_pool = new ebuffer[pool_size];
        auto size = pool_size / 8 + (pool_size % 8 == 0 ? 0 : 1);
        _bitmap_buffer = new uint8_t [size];
        buffer_bitmap.init(_bitmap_buffer, pool_size);
    }
    catch (...)
    {
        str_throw_hmnx_error("Device initialization failed!");
    }
}

uint64_t eblock::next_free_buffer()
{
    int8_t retry_timeout = 3;
    retry:
    try
    {
        return buffer_bitmap.next_free_node();
    }
    catch (hmnx_error_t & err)
    {
        if (err.getx_errcode() == BUFFER_DEPLETED && retry_timeout > 0)
        {
            free_buffer(access_list.size() / 2);
            hmnx_usleep(1000);
            retry_timeout--;
            goto retry;
        }

        throw ;
    }
    catch (...)
    {
        throw ;
    }
}

void eblock::free_buffer(uint64_t size)
{
    if (access_list.empty())
    {
        return;
    }

    if (size == 0)
    {
        size = 1;
    }

    if (size > access_list.size())
    {
        size = access_list.size();
    }

    auto it = access_list.begin();

    for (size_t i = 0; i < size; i++)
    {
        auto & buffer = *buffer_pool.at(*it);

        // erase buffer
        buffer_bitmap.set(buffer.index, false);
        buffer_pool.erase(*it);
        access_list.erase(it);
    }
}

eblock::~eblock()
{
    delete[] _bitmap_buffer;
    delete[] _raw_buffer_pool;
}

bool if_snapshoted_on(uint16_t version, block_attribute_t attr)
{
    if (version == 0)
    {
        code_throw_hmnx_error(INVALID_SNAPSHOT_VERSION);
    }

    version -= 1;
    uint8_t off = version / 8;
    uint8_t bit_off = version % 8;
    uint8_t dummy = 0x01 << bit_off;
    return attr.snapshot_log[off] & dummy;
}

void change_snapshot_status(bool status, uint16_t version, block_attribute_t & attr)
{
    if (version == 0)
    {
        code_throw_hmnx_error(INVALID_SNAPSHOT_VERSION);
    }

    version -= 1;
    uint8_t off = version / 8;
    uint8_t bit_off = version % 8;
    uint8_t dummy = 0x01 << bit_off;
    uint8_t dummy_comp = ~dummy;
    uint8_t null_pir = attr.snapshot_log[off] & dummy_comp; // delete the bit
    if (status)
    {
        null_pir |= dummy; // add our status
    }
    attr.snapshot_log[off] = null_pir;
}
