#include <shs/device.h>
#include <emulated/ebuffer.h>
#include <shs/hmnx_error.h>
#include <shs/journal.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

//ebuffer::ebuffer(device &ops_dev,
//                 journal & _fs_journal,
//                 uint64_t _logic_block,
//                 uint64_t block_size,
//                 uint64_t _logic_starting_block)
//:
//    ops_device(&ops_dev),
//    fs_journal(&_fs_journal),
//    logic_block(_logic_block - 1),
//    starting_raw_block((_logic_block - 1) * block_size + _logic_starting_block),
//    raw_block_count(block_size)
//{
//}


size_t ebuffer::read(char *buffer, uint64_t size, uint64_t offset)
{
    if (!size)
    {
        return 0;
    }

    uint64_t logic_buffer_size = raw_block_count * BLOCK_SIZE;
    if (offset >= logic_buffer_size)
    {
        return 0;
    }

    if ((size + offset) > logic_buffer_size)
    {
        size = logic_buffer_size - offset;
    }

    size_t read_off = 0;
    uint64_t starting_buffer = offset / BLOCK_SIZE;
    uint64_t starting_buffer_offset = offset % BLOCK_SIZE;
    uint64_t starting_buffer_read_len = MIN(BLOCK_SIZE - starting_buffer_offset, size);
    uint64_t remaining_full_buffer_count = (size - starting_buffer_read_len) / BLOCK_SIZE;
    uint64_t last_buffer_read_len = (size - starting_buffer_read_len) % BLOCK_SIZE;

    // starting buffer
    auto &_1st_buffer = ops_device->alloc_buffer(starting_raw_block + starting_buffer);
    read_off = _1st_buffer.read(buffer, starting_buffer_read_len, starting_buffer_offset);
    _1st_buffer.drop();

    // full buffers
    for (uint64_t i = 1; i <= remaining_full_buffer_count; i++)
    {
        auto & raw_buffer = ops_device->alloc_buffer(starting_raw_block + starting_buffer + i);
        read_off += raw_buffer.read(buffer + read_off, BLOCK_SIZE, 0);
        raw_buffer.drop();
    }

    // last buffer
    if (last_buffer_read_len)
    {
        auto &last_buffer = ops_device->alloc_buffer(starting_raw_block + starting_buffer
                                                    + remaining_full_buffer_count + 1);
        read_off += last_buffer.read(buffer + read_off, last_buffer_read_len, 0);
        last_buffer.drop();
    }
    return read_off;
}


size_t ebuffer::write(const char *buffer, uint64_t size, uint64_t offset)
{
    if (!size)
    {
        return 0;
    }

    fs_journal->command(MODIFY_BLOCK_CONT, MK_64bit_PARAM(logic_block + 1));

    uint64_t logic_buffer_size = raw_block_count * BLOCK_SIZE;
    if (offset >= logic_buffer_size)
    {
        return 0;
    }

    if ((size + offset) > logic_buffer_size)
    {
        size = logic_buffer_size - offset;
    }

    size_t write_off = 0;
    uint64_t starting_buffer = offset / BLOCK_SIZE;
    uint64_t starting_buffer_offset = offset % BLOCK_SIZE;
    uint64_t starting_buffer_write_len = MIN(BLOCK_SIZE - starting_buffer_offset, size);
    uint64_t remaining_full_buffer_count = (size - starting_buffer_write_len) / BLOCK_SIZE;
    uint64_t last_buffer_write_len = (size - starting_buffer_write_len) % BLOCK_SIZE;

    // starting buffer
    auto &_1st_buffer = ops_device->alloc_buffer(starting_raw_block + starting_buffer);
    write_off = _1st_buffer.write(buffer, starting_buffer_write_len, starting_buffer_offset);
    _1st_buffer.drop();

    // full buffers
    for (uint64_t i = 1; i <= remaining_full_buffer_count; i++)
    {
        auto & raw_buffer = ops_device->alloc_buffer(starting_raw_block + starting_buffer + i);
        write_off += raw_buffer.write(buffer + write_off, BLOCK_SIZE, 0);
        raw_buffer.drop();
    }

    // last buffer
    if (last_buffer_write_len)
    {
        auto &last_buffer = ops_device->alloc_buffer(starting_raw_block + starting_buffer
                                                    + remaining_full_buffer_count + 1);
        write_off += last_buffer.write(buffer + write_off, last_buffer_write_len, 0);
        last_buffer.drop();
    }

    fs_journal->command(OPERATION_FINISHED);

    return write_off;
}

void ebuffer::be_zero()
{
    fs_journal = nullptr;
    ops_device = nullptr;
    logic_block = 0;
    starting_raw_block = 0;
    raw_block_count = 0;
    index = 0;
}
