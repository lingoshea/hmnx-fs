#include <fs.h>
#include <iostream>
#include <cstdlib>
#include <shs/device.h>
#include <shs/hmnx_error.h>
#include <shs/journal.h>
#include <iomanip>
#include <getopt.h>
#include <unordered_map>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define VERSION "cat_fsjour " GENERAL_VERSION

operation_t opcode [] = {
        OPERATION_FINISHED,
        _PUSH_BLOCK_NUMBER,
        _PUSH_UINT32,
        _PUSH_CHECKSUM,

        FILESYSTEM_MOUNT,
        FILESYSTEM_UNMOUNT,
        MKREDIRECT,
        ADD_ZERO_ENTRY,
        SNAPSHOT,
        DELETE_SNAPSHOT,
        MODIFY_BLOCK_ATTR,
        MODIFY_BLOCK_CONT,
        FILESYSTEM_ERROR,
        ADD_SNAPSHOT_HISTORY,
        DEL_SNAPSHOT_HISTORY,
        ADD_REDIRECT_ENTRY,
        DEL_REDIRECT_ENTRY,
        ALLOCATE_INODE,
        DEALLOCATE_INODE,
        ADD_INODE_BLOCK_ENTRY,
        DEL_INODE_BLOCK_ENTRY,
        MODIFY_INODE_CONT,
        MODIFY_OCCUPATION_STATUS,
        CHECKSUM_IN_BLOCK,
        FILESYSTEM_SYNC
};

inline uint64_t prase_uint32(uint32_t h32, uint32_t l32)
{
    uint64_t ret = h32;
    ret <<= 32;
    ret |= l32;
    return ret;
}

bool dump = false;
char * device_name = nullptr;

std::unordered_map < uint32_t /* opcode*/, std::vector < uint32_t > > ops_stack;

#define RED         "\033[01;31m"
#define GREEN       "\033[01;32m"
#define YELLOW      "\033[01;33m"
#define BLUE        "\033[01;36m"
#define NO_COLOR    "\033[0m"

std::string lltostr_with_scale(uint64_t);

void print_sb_info(super_block_t sb)
{
    uint64_t _fs_size, _journaling_size, _minimap_size, _bitmap_size, _snapshot_history_size, _logic_size;
    uint64_t sig_block_size = BLOCK_SIZE / 1024;

    // the following size is in Kb
    _fs_size                = sb.filesystem_block_count * sig_block_size;
    _journaling_size        = sb.filesystem_journal_block_count * sig_block_size;
    _minimap_size           = sb.mini_bitmap_block_count * sig_block_size;
    _bitmap_size            = sb.bitmap_block_count * sig_block_size;
    _snapshot_history_size  = sb.snapshot_history_block_count * sig_block_size;
    _logic_size             = sb.logic_block_count * sb.logic_block_size * sig_block_size;

    printf("Filesystem basic information:\n"
           "      Super block structure size:         \t%lu bytes\n"
           "   *  Magic number:                       \t0x%lX\n"
           "   *  %1luKb block count:                    \t%lu (%s)\n"
           "  ==  Journaling starts from:             \t%lu\n"
           "      %1luKb journaling block count:         \t%lu (%s)\n"
           "  ==  Minibitmap starts from:             \t%lu\n"
           "      %1luKb Minibitmap block count:         \t%lu (%s)\n"
           "  ==  Bitmap starts from:                 \t%lu\n"
           "      %1luKb bitmap block count:             \t%lu (%s)\n"
           "  ==  Snapshot history starts from:       \t%lu\n"
           "      %1luKb snapshot history block count:   \t%lu (%s)\n"
           "  ==  Logic block starts from:            \t%lu\n"
           "      Logic block count:                  \t%lu (%s)\n"
           "      Logic block size (in %dKb):         \t%lu\n"
           "  **  Logic space efficiency:             \t%lf %%\n"
           "  **  Filesystem space efficiency:        \t%lf %%\n",
           sizeof (sb),
           sb.magic,
           sig_block_size,
           sb.filesystem_block_count, lltostr_with_scale(_fs_size).c_str(),
           sb.filesystem_journal_starting_block, sig_block_size,
           sb.filesystem_journal_block_count, lltostr_with_scale(_journaling_size).c_str(),
           sb.mini_bitmap_starting_block, sig_block_size,
           sb.mini_bitmap_block_count, lltostr_with_scale(_minimap_size).c_str(),
           sb.bitmap_starting_block, sig_block_size,
           sb.bitmap_block_count, lltostr_with_scale(_bitmap_size).c_str(),
           sb.snapshot_history_starting_block, sig_block_size,
           sb.snapshot_history_block_count, lltostr_with_scale(_snapshot_history_size).c_str(),
           sb.logic_block_starting_block,
           sb.logic_block_count, lltostr_with_scale(_logic_size).c_str(),
           (ALIGNED_SIZE) / 1024,
           sb.logic_block_size,
           (double)(sb.logic_block_size * sb.logic_block_count)
           / (double)sb.filesystem_block_count * 100.00,
           (double)(sb.logic_block_size * sb.logic_block_count
                    + sb.logic_block_starting_block)
           / (double)sb.filesystem_block_count * 100.00);
}

std::string lltostr_with_scale(uint64_t num)
{
    static char buffer [BLOCK_SIZE] { };
    std::string suffix = " ";
    uint64_t scale = 1;

    if (num < 1024)
    {
        suffix += "Kb";
    }
    else if (num < 1024 * 1024)
    {
        scale = 1024;
        suffix += "Mb";
    }
    else if (num < 1024 * 1024 * 1024)
    {
        scale = 1024 * 1024;
        suffix += "Gb";
    }
    else
    {
        scale = 1024 * 1024 * 1024;
        suffix += "Tb";
    }

    sprintf(buffer, "%.2Lf%s", num / ((long double)scale), suffix.c_str());

    return std::string(buffer);
}

void help(char ** argv)
{
    fprintf(stdout,     VERSION "\n"
                        "Usage: %s [OPTIONS] DEVICE\n"
                        "       -h, --help          Show this help message.\n"
                        "       -d, --dump          Dump raw journaling data instead of interpreting.\n",
            *argv);
}

void phrase_opt(int argc, char ** argv)
{
    int opt;
    int option_index;

    static struct option long_options[] =
            {
                    {"dump",        no_argument,       nullptr, 'd'},
                    {"help",        no_argument,       nullptr, 'h'},
                    {nullptr, 0, nullptr, 0}
            };

    while ((opt = getopt_long(argc, argv, "hd", long_options, &option_index)) != -1)
    {
        switch (opt)
        {
            case 'd':
                dump = true;
                break;
            case 'h':
                help(argv);
                exit(EXIT_SUCCESS);
            case '?':
                /* getopt_long already printed an error message. */
            default: /* '?' */
                help(argv);
                exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc)
    {
        fprintf(stderr, "Missing path to device after options.\n");
        help(argv);
        exit(EXIT_FAILURE);
    }

    device_name = argv[optind];
}

[[noreturn]]
void interpreter(journal & _fs_journal);

int main(int argc, char ** argv)
{
    set_limit(1024 * 1024 * 64);

    phrase_opt(argc, argv);

    try
    {
        device filesystem;
        filesystem.set_bpoll_sz(32);
        filesystem.open(device_name);

        // super block
        super_block_t sb { };

        auto & super_block_buffer = filesystem.alloc_buffer(0);
        super_block_buffer.read((char*)&sb, sizeof (sb), 0);
        super_block_buffer.drop();

        if (!(sb.magic == HMNXFS_MAGIC && ~sb.magic_comp == HMNXFS_MAGIC))
        {
            str_throw_hmnx_error("Unknown filesystem");
        }

        print_sb_info(sb);


        journal _fs_journal(filesystem._file_info.fd,
                           sb.filesystem_journal_starting_block * (BLOCK_SIZE / ALIGNED_SIZE),
                           sb.filesystem_journal_block_count * (BLOCK_SIZE / ALIGNED_SIZE),
                           true);

        std::cout << "--- start of filesystem journal ---\n" << std::endl;

        if (dump)
        {
            while (true)
            {
                std::cout << std::hex << std::setw(8) << std::setfill('0')
                      << _fs_journal.pop(true) << "\t"
                      << std::hex << std::setw(8) << std::setfill('0')
                      << _fs_journal.pop(true) << "\t"
                      << std::hex << std::setw(8) << std::setfill('0')
                      << _fs_journal.pop(true) << "\t"
                      << std::hex << std::setw(8) << std::setfill('0')
                      << _fs_journal.pop(true) << std::endl;
            }
        }
        else
        {
            interpreter(_fs_journal);
        }

    }
    catch (hmnx_error_t & err)
    {
        if (err.getx_errcode() == EMPTY_BUFFER)
        {
            std::cout << "\n--- end of filesystem journal ---" << std::endl;
            return EXIT_SUCCESS;
        }

        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}

#define NANO 1000000000L

// buf needs to store 30 characters
int timespec2str(char *buf, uint len, struct timespec *ts)
{
    int ret;
    struct tm t{};

    tzset();
    if (localtime_r(&(ts->tv_sec), &t) == nullptr)
        return 1;

    ret = strftime(buf, len, "%F %T", &t);
    if (ret == 0)
        return 2;
    len -= ret - 1;

    ret = snprintf(&buf[strlen(buf)], len, ".%09ld", ts->tv_nsec);
    if (ret >= len)
        return 3;

    return 0;
}

void show_time(struct timespec & ts)
{
    clockid_t clk_id = CLOCK_REALTIME;
    const uint TIME_FMT = strlen("2012-12-31 12:59:59.123456789") + 1;
    char timestr[TIME_FMT];

    timespec2str(timestr, sizeof(timestr), &ts);
    std::cout << timestr;
}

[[noreturn]]
void interpreter(journal & _fs_journal)
{
    std::string prefix;
    
    auto can_find_opcode = [&](uint32_t cmd)->uint32_t
    {
        for (auto i : opcode)
        {
            if (i == cmd)
            {
                if (_fs_journal.pop(true) == ~cmd)
                {
                    return cmd;
                }
            }
        }

        return 0;
    };

    auto _pop4_time = [&]()
    {
        timespec time{};
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);
        uint32_t h2_32 = _fs_journal.pop(true);
        uint32_t l2_32 = _fs_journal.pop(true);

        time.tv_sec = (int64_t)prase_uint32(h32, l32);
        time.tv_nsec = (int64_t)prase_uint32(h2_32, l2_32);

        show_time(time);
        std::cout << "\t: ";
    };

    auto _pop0 = [&](const std::string & name)
    {
        _pop4_time();
        std::cout << prefix << name << std::endl;
    };


    auto _pop1 = [&](const std::string & name)
    {
        uint32_t h32 = _fs_journal.pop(true);

        _pop4_time();
        std::cout << prefix << name << "\t" << h32 << std::endl;
    };

    auto _pop2_64 = [&](const std::string & name)
    {
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);

        _pop4_time();
        std::cout << prefix << name << "\t" << prase_uint32(h32, l32) << std::endl;
    };

    auto _pop3_64_n_32 = [&](const std::string & name)
    {
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);
        uint64_t m32 = _fs_journal.pop(true);

        _pop4_time();
        std::cout << prefix << name << "\t" << prase_uint32(h32, l32) << "\t" << m32 << std::endl;
    };

    auto _pop4_64_n_64 = [&](const std::string & name, bool show_in_hex = false)
    {
        timespec time{};
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);

        uint32_t h2_32 = _fs_journal.pop(true);
        uint32_t l2_32 = _fs_journal.pop(true);

        _pop4_time();
        std::cout << prefix << name << "\t"
                  << prase_uint32(h32, l32) << "\t";

        if (show_in_hex)
        {
            std::cout << std::hex << std::setw(sizeof (uint64_t) * 2) << prase_uint32(h2_32, l2_32)
                      << std::dec << std::endl;
        }
        else
        {
            std::cout << prase_uint32(h2_32, l2_32) << std::endl;
        }

    };

    while (true)
    {
        uint32_t cmd = _fs_journal.pop(true);
        auto ops = can_find_opcode(cmd);
        switch (ops)
        {
            case _PUSH_BLOCK_NUMBER:
            {
                _pop2_64("_PUSH_BLOCK_NUMBER");
                break;
            }
            case _PUSH_UINT32:
            {
                _pop1("_PUSH_UINT32");
                break;
            }
            case _PUSH_CHECKSUM:
            {
                _pop2_64("_PUSH_CHECKSUM");
                break;
            }
            case OPERATION_FINISHED:
                _pop0(GREEN "OPERATION_FINISHED" NO_COLOR);

                if (!prefix.empty())
                {
                    prefix.pop_back();
                }

                break;

            case FILESYSTEM_MOUNT:
            {
                prefix.clear();
                _pop0(BLUE "FILESYSTEM_MOUNT" NO_COLOR);
                break;
            }
            case FILESYSTEM_UNMOUNT:
            {
                prefix.clear();
                _pop0(BLUE "FILESYSTEM_UNMOUNT" NO_COLOR);
                break;
            }
            case MKREDIRECT:
            {
                prefix += "\t";
                _pop2_64("MKREDIRECT");
                break;
            }
            case ADD_ZERO_ENTRY:
            {
                prefix += "\t";
                _pop3_64_n_32("ADD_ZERO_ENTRY");
                break;
            }
            case SNAPSHOT:
            {
                prefix += "\t";
                _pop0("SNAPSHOT");
                break;
            }
            case DELETE_SNAPSHOT:
            {
                prefix += "\t";
                _pop1("DELETE_SNAPSHOT");
                break;
            }
            case MODIFY_BLOCK_ATTR:
            {
                prefix += "\t";
                _pop2_64("MODIFY_BLOCK_ATTR");
                break;
            }
            case MODIFY_BLOCK_CONT:
            {
                prefix += "\t";
                _pop2_64("MODIFY_BLOCK_CONT");
                break;
            }
            case FILESYSTEM_ERROR:
            {
                prefix.clear();
                _pop1(RED "FILESYSTEM_ERROR" NO_COLOR);
                break;
            }
            case ADD_SNAPSHOT_HISTORY:
            {
                prefix += "\t";
                _pop1("ADD_SNAPSHOT_HISTORY");
                break;
            }
            case DEL_SNAPSHOT_HISTORY:
            {
                prefix += "\t";
                _pop1("DEL_SNAPSHOT_HISTORY");
                break;
            }
            case ADD_REDIRECT_ENTRY:
            {
                prefix += "\t";
                _pop3_64_n_32("ADD_REDIRECT_ENTRY");
                break;
            }
            case DEL_REDIRECT_ENTRY:
            {
                prefix += "\t";
                _pop3_64_n_32("DEL_REDIRECT_ENTRY");
                break;
            }
            case ALLOCATE_INODE:
            {
                prefix += "\t";
                _pop0("ALLOCATE_INODE");
                break;
            }
            case DEALLOCATE_INODE:
            {
                prefix += "\t";
                _pop2_64("DEALLOCATE_INODE");
                break;
            }
            case ADD_INODE_BLOCK_ENTRY:
            {
                prefix += "\t";
                _pop4_64_n_64("ADD_INODE_BLOCK_ENTRY");
                break;
            }
            case DEL_INODE_BLOCK_ENTRY:
            {
                prefix += "\t";
                _pop4_64_n_64("DEL_INODE_BLOCK_ENTRY");
                break;
            }
            case MODIFY_INODE_CONT:
            {
                prefix += "\t";
                _pop2_64("MODIFY_INODE_CONT");
                break;
            }
            case MODIFY_OCCUPATION_STATUS:
            {
                prefix += "\t";
                _pop2_64("MODIFY_OCCUPATION_STATUS");
                break;
            }
            case FILESYSTEM_SYNC:
            {
                prefix += "\t";
                _pop0("FILESYSTEM_SYNC");
                break;
            }
            case CHECKSUM_IN_BLOCK:
            {
                _pop4_64_n_64(YELLOW "CHECKSUM_IN_BLOCK" NO_COLOR, true);
                break;
            }
            default:
                continue;
        }
    }
}
