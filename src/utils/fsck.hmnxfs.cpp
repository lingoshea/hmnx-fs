#include <fs.h>
#include <iostream>
#include <cstdlib>
#include <shs/device.h>
#include <shs/hmnx_error.h>
#include <shs/journal.h>
#include <getopt.h>
#include <xxhash64.h>
#include <emulated/eblock.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define VERSION "fsck.hmnxfs " GENERAL_VERSION

operation_t opcode [] = {
        OPERATION_FINISHED,
        _PUSH_BLOCK_NUMBER,
        _PUSH_UINT32,
        _PUSH_CHECKSUM,

        FILESYSTEM_MOUNT,
        FILESYSTEM_UNMOUNT,
        MKREDIRECT,
        ADD_ZERO_ENTRY,
        SNAPSHOT,
        DELETE_SNAPSHOT,
        MODIFY_BLOCK_ATTR,
        MODIFY_BLOCK_CONT,
        FILESYSTEM_ERROR,
        ADD_SNAPSHOT_HISTORY,
        DEL_SNAPSHOT_HISTORY,
        ADD_REDIRECT_ENTRY,
        DEL_REDIRECT_ENTRY,
        ALLOCATE_INODE,
        DEALLOCATE_INODE,
        ADD_INODE_BLOCK_ENTRY,
        DEL_INODE_BLOCK_ENTRY,
        MODIFY_INODE_CONT,
        MODIFY_OCCUPATION_STATUS,
        CHECKSUM_IN_BLOCK,
        FILESYSTEM_SYNC
};

inline uint64_t prase_uint32(uint32_t h32, uint32_t l32)
{
    uint64_t ret = h32;
    ret <<= 32;
    ret |= l32;
    return ret;
}

char * device_name = nullptr;

std::unordered_map < long double, std::vector < uint32_t > /* opcode and args */ > ops_stack;
std::unordered_map < uint64_t /* block num */, uint64_t /* last block hash */> block_hash;
std::vector < uint64_t > block_context_lost;
std::vector < uint64_t > orphans;

void print_sb_info(super_block_t sb)
{
    printf("Filesystem basic information:\n"
           "      Super block structure size:         %lu bytes\n"
           "   *  Magic number:                       0x%lX\n"
           "   *  %02dKb block count:                   %lu\n"
           "  ==  Journaling starts from:             %lu\n"
           "      %02dKb journaling block count:        %lu\n"
           "  ==  Minibitmap starts from:             %lu\n"
           "      %02dKb Minibitmap block count:        %lu\n"
           "  ==  Bitmap starts from:                 %lu\n"
           "      %02dKb bitmap block count :           %lu\n"
           "  ==  Snapshot history starts from:       %lu\n"
           "      %02dKb snapshot history block count:  %lu\n"
           "  ==  Logic block starts from:            %lu\n"
           "      Logic block count:                  %lu\n"
           "      Logic block size:                   %lu\n"
           "  **  Logic space efficiency:             %lf %%\n"
           "  **  Filesystem space efficiency:        %lf %%\n"
           "  **  Allocated logic block count:        %lu\n"
           "  **  Allocated inode count:              %lu\n",
           sizeof (sb),
           sb.magic,
           BLOCK_SIZE / 1024,
           sb.filesystem_block_count,
           sb.filesystem_journal_starting_block,
           BLOCK_SIZE / 1024,
           sb.filesystem_journal_block_count,
           sb.mini_bitmap_starting_block,
           BLOCK_SIZE / 1024,
           sb.mini_bitmap_block_count,
           sb.bitmap_starting_block,
           BLOCK_SIZE / 1024,
           sb.bitmap_block_count,
           sb.snapshot_history_starting_block,
           BLOCK_SIZE / 1024,
           sb.snapshot_history_block_count,
           sb.logic_block_starting_block,
           sb.logic_block_count,
           sb.logic_block_size,
           (double)(sb.logic_block_size * sb.logic_block_count)
           / (double)sb.filesystem_block_count * 100.00,
           (double)(sb.logic_block_size * sb.logic_block_count
                    + sb.logic_block_starting_block)
           / (double)sb.filesystem_block_count * 100.00,
           sb.allocated_logic_block_count,
           sb.allocated_inode_count
    );
}

void help(char ** argv)
{
    fprintf(stdout,     VERSION "\n"
                        "Usage: %s [OPTIONS] DEVICE\n"
                        "       -h, --help          Show this help message.\n",
            *argv);
}

void phrase_opt(int argc, char ** argv)
{
    int opt;
    int option_index;

    static struct option long_options[] =
            {
                    {"help",        no_argument,       nullptr, 'h'},
                    {nullptr, 0, nullptr, 0}
            };

    while ((opt = getopt_long(argc, argv, "h", long_options, &option_index)) != -1)
    {
        switch (opt)
        {
            case 'h':
                help(argv);
                exit(EXIT_SUCCESS);
            case '?':
                /* getopt_long already printed an error message. */
            default: /* '?' */
                help(argv);
                exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc)
    {
        fprintf(stderr, "Missing path to device after options.\n");
        help(argv);
        exit(EXIT_FAILURE);
    }

    device_name = argv[optind];
}

template < typename T >
typename std::vector<T>::iterator can_find(std::vector < T > & list, T key)
{
    for (auto k = list.begin(); k < list.end(); k++)
    {
        if (*k == key)
        {
            return k;
        }
    }

    return list.end();
}

void interpreter(journal & _fs_journal);

int main(int argc, char ** argv)
{
    set_limit(1024 * 1024 * 64);

    phrase_opt(argc, argv);

    try
    {
        device filesystem;
        filesystem.set_bpoll_sz(32);
        filesystem.open(device_name);

        // super block
        super_block_t sb { };

        auto & super_block_buffer = filesystem.alloc_buffer(0);
        super_block_buffer.read((char*)&sb, sizeof (sb), 0);
        super_block_buffer.drop();

        if (!(sb.magic == HMNXFS_MAGIC && ~sb.magic_comp == HMNXFS_MAGIC))
        {
            std::cerr << "Unknown filesystem" << std::endl;
        }

        print_sb_info(sb);

        journal _fs_journal(filesystem._file_info.fd,
                            sb.filesystem_journal_starting_block * (BLOCK_SIZE / ALIGNED_SIZE),
                            sb.filesystem_journal_block_count * (BLOCK_SIZE / ALIGNED_SIZE),
                            true);

        interpreter(_fs_journal);

        eblock logic_block(filesystem, _fs_journal,
               sb.mini_bitmap_starting_block,
               sb.mini_bitmap_block_count,
               sb.bitmap_starting_block,
               sb.bitmap_block_count,
               sb.snapshot_history_starting_block,
               sb.snapshot_history_block_count,
               sb.logic_block_starting_block,
               sb.logic_block_count,
               sb.logic_block_size);

        // check block hash
        for (auto & it : ops_stack)
        {
            if (*it.second.begin() == CHECKSUM_IN_BLOCK
            && block_hash.find(*it.second.begin()) == block_hash.end()
            )
            {
                auto block = prase_uint32(it.second[1], it.second[2]);
                auto checksum = prase_uint32(it.second[3], it.second[4]);
                block_hash.emplace(block, checksum);
            }
        }

        char c_buff [BLOCK_SIZE] { };

        // check hash
        for (auto & itr : block_hash)
        {
            auto & buffer = filesystem.alloc_buffer(itr.first);
            buffer.read(c_buff, sizeof (c_buff), 0);
            buffer.drop();

            uint64_t hash = XXHash64::hash(c_buff, sizeof (c_buff), HMNXFS_CHECKSUM_SEED);

            if (hash != itr.second)
            {
                block_context_lost.emplace_back(itr.first);
            }
        }

        if (block_context_lost.empty())
        {
            std::cout << "Block context clean ..." << std::endl;
        }

        // check bitmap consistency
        for (uint64_t i = 1; i < sb.logic_block_count; i++)
        {
            auto attr = logic_block.block_bitmap.read_attribute(i);
            auto occ_bit = logic_block.block_bitmap.if_occupied(i);

            if ((occ_bit || attr.block_type != BLOCK_UNUSED)
            && !(occ_bit && attr.block_type != BLOCK_UNUSED))
            {
                if (!occ_bit || attr.block_type == BLOCK_UNUSED)
                {
                    if (!occ_bit)
                    {
                        attr.block_type = BLOCK_UNUSED;
                    }

                    if (attr.block_type != BLOCK_UNUSED)
                    {
                        logic_block.block_bitmap.set_occupation_status(false, i);
                    }
                }

                logic_block.block_bitmap.write_attribute(attr, i);
            }
            else
            {
                if (attr.block_type != BLOCK_INODE && attr.block_type != BLOCK_UNUSED)
                {
                    orphans.emplace_back(i);
                }
                else if (attr.block_type == BLOCK_INODE)
                {
                    // FIXME: inode filed
                    hmnx_inode inode{};
                    logic_block.direct_access(i).read((char*)&inode, sizeof (inode), 0);
                    for (uint64_t j = 0; j < MIN(512, inode.file_len / (BLOCK_SIZE * sb.logic_block_size) + 1); j++)
                    {
                        auto itr = can_find(orphans, inode.file_zone[i]);
                        if (itr != orphans.end())
                        {
                            orphans.erase(itr);
                        }
                    }
                }
            }
        }

        block_attribute_t empty_attr { };

        if (orphans.empty())
        {
            std::cout << "Data block clean ..." << std::endl;
        }
        else
        {
            // TODO: clean up
        }

        for (auto blk : block_context_lost)
        {
            // TODO: Clean up
        }
    }
    catch (hmnx_error_t & err)
    {
        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}

// buf needs to store 30 characters
size_t timespec2str(char *buf, uint len, struct timespec *ts)
{
    size_t ret;
    struct tm t{};

    tzset();
    if (localtime_r(&(ts->tv_sec), &t) == nullptr)
        return 1;

    ret = strftime(buf, len, "%F %T", &t);
    if (ret == 0)
        return 2;
    len -= ret - 1;

    ret = snprintf(&buf[strlen(buf)], len, ".%09ld", ts->tv_nsec);
    if (ret >= len)
        return 3;

    return 0;
}


void interpreter(journal & _fs_journal)
{
    std::string prefix;

    auto can_find_opcode = [&](uint32_t cmd)->uint32_t
    {
        for (auto i : opcode)
        {
            if (i == cmd)
            {
                if (_fs_journal.pop(true) == ~cmd)
                {
                    return cmd;
                }
            }
        }

        return 0;
    };

    auto _pop4_time = [&](uint32_t ops)->long double
    {
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);
        uint32_t h2_32 = _fs_journal.pop(true);
        uint32_t l2_32 = _fs_journal.pop(true);
        auto usec = prase_uint32(h2_32, l2_32);
        uint32_t i = 1;
        long double time = 0.00;
        while (usec > 0)
        {
            uint32_t num = usec % 10;
            time += num * 0.1 * i;
            i++;
            usec /= 10;
        }

        time += prase_uint32(h32, l32);

        ops_stack.emplace(time, std::vector<uint32_t>{ops});

        return time;
    };

    auto _pop0 = [&](uint32_t ops)
    {
        _pop4_time(ops);
    };


    auto _pop1 = [&](uint32_t ops)
    {
        uint32_t h32 = _fs_journal.pop(true);
        ops_stack[_pop4_time(ops)].emplace_back(h32);
    };

    auto _pop2 = [&](uint32_t ops)
    {
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);

        auto time = _pop4_time(ops);
        ops_stack[time].emplace_back(h32);
        ops_stack[time].emplace_back(l32);
    };

    auto _pop3 = [&](uint32_t ops)
    {
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);
        uint64_t m32 = _fs_journal.pop(true);

        auto time = _pop4_time(ops);
        ops_stack[time].emplace_back(h32);
        ops_stack[time].emplace_back(l32);
        ops_stack[time].emplace_back(m32);
    };

    auto _pop4 = [&](uint32_t ops)
    {
        uint32_t h32 = _fs_journal.pop(true);
        uint32_t l32 = _fs_journal.pop(true);

        uint32_t h2_32 = _fs_journal.pop(true);
        uint32_t l2_32 = _fs_journal.pop(true);

        auto time = _pop4_time(ops);

        ops_stack[time].emplace_back(h32);
        ops_stack[time].emplace_back(l32);
        ops_stack[time].emplace_back(h2_32);
        ops_stack[time].emplace_back(l2_32);
    };

    try {


        while (true) {
            uint32_t cmd = _fs_journal.pop(true);
            auto ops = can_find_opcode(cmd);

            uint32_t arg_count = ops & 0x0FFFFFFF;

            switch (arg_count) {
                case 0:
                    _pop0(ops);
                    break;
                case 1:
                    _pop1(ops);
                    break;
                case 2:
                    _pop2(ops);
                    break;
                case 3:
                    _pop3(ops);
                    break;
                case 4:
                    _pop4(ops);
                    break;
                default:
                    continue;
            }
        }
    }
    catch (...)
    {
        // ignore any exceptions
    }
}

