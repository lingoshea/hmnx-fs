#include <fs.h>
#include <iostream>
#include <cstdlib>
#include <sys/ioctl.h>
#include <getopt.h>
#include <sys/types.h>
#include <dirent.h>
#include <shs/hmnx_error.h>
#include <cstring>
#include <unistd.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define VERSION "hmnx_send_cmd " GENERAL_VERSION

#define RED         "\033[01;31m"
#define GREEN       "\033[01;32m"
#define YELLOW      "\033[01;33m"
#define BLUE        "\033[01;36m"
#define NO_COLOR    "\033[0m"

char * device_name = nullptr;
int cmd = 0;
uint64_t args[4];

void help(char ** argv)
{
    fprintf(stdout,     VERSION "\n"
                        "Usage: %s [OPTIONS, VALUE] DEVICE\n"
                        "       -h, --help              Show this help message.\n"
                        "       -d, --defragmentation   Start defragmentation.\n"
                        "       -c, --create-snapshot   Create a snapshot.\n"
                        "       -e, --erase-snapshot    Delete a snapshot.",
            *argv);
}

void phrase_opt(int argc, char ** argv)
{
    int opt;
    int option_index;

    static struct option long_options[] =
            {
                    {"help",            no_argument,       nullptr, 'h'},
                    {"defragmentation", no_argument,       nullptr, 'd'},
                    {"create-snapshot", no_argument,       nullptr, 'c'},
                    {"erase-snapshot",  required_argument, nullptr, 'e'},
                    {nullptr, 0, nullptr, 0}
            };

    while ((opt = getopt_long(argc, argv, "hdce:", long_options, &option_index)) != -1)
    {
        switch (opt)
        {
            case 'd':
                cmd = HMNX_IOCTL_DEFRAGMENTATION;
                break;
            case 'c':
                cmd = HMNX_IOCTL_CREATE_SNAPSHOT;
                break;
            case 'e':
                cmd = HMNX_IOCTL_DELETE_SNAPSHOT;
                args[0] = strtol(optarg, nullptr, 10);
                break;
            case 'h':
                help(argv);
                exit(EXIT_SUCCESS);
            case '?':
                /* getopt_long already printed an error message. */
            default: /* '?' */
                help(argv);
                exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc)
    {
        fprintf(stderr, "Missing path to device after options.\n");
        help(argv);
        exit(EXIT_FAILURE);
    }

    device_name = argv[optind];
}

int main(int argc, char ** argv)
{
    phrase_opt(argc, argv);

    try
    {
        if (chdir(device_name) == -1)
        {
            code_throw_hmnx_error(-1);
        }

        auto dir_fd = opendir(device_name);
        if (!dir_fd)
        {
            str_throw_hmnx_error("Cannot open root directory!");
        }

        auto fd = open(".hmnx_cmd", O_WRONLY);
        if (fd == -1)
        {
            code_throw_hmnx_error(-1);
        }

        auto err_code = ioctl(fd, cmd, args);

        if (err_code != 0)
        {
            code_throw_hmnx_error(err_code);
        }

        return EXIT_SUCCESS;

        str_throw_hmnx_error("Empty filesystem!");
    }
    catch (hmnx_error_t & error)
    {
        std::cerr << error.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }
}
