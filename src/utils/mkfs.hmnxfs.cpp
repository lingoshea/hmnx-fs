#include <fs.h>
#include <getopt.h>
#include <iostream>
#include <cstdlib>
#include <shs/device.h>
#include <shs/hmnx_error.h>
#include <emulated/ebitmap.h>
#include <cstring>
#include <shs/hmnx_time.h>
#include <unistd.h>
#include <sys/types.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

gid_t       format_gid = 0;
uid_t       format_uid = 0;
uint64_t    logic_block_size    = BLOCK_SIZE / ALIGNED_SIZE;
uint64_t    cache_space_size    = 10240;
char *      device_name         = nullptr;
bool        clear_bitmap        = false;
bool        clear_journaling    = false;
#define VERSION "mkfs.hmnxfs " GENERAL_VERSION

std::string lltostr_with_scale(uint64_t);


void print_sb_info(super_block_t sb)
{
    uint64_t _fs_size, _journaling_size, _minimap_size, _bitmap_size, _snapshot_history_size, _logic_size;
    uint64_t sig_block_size = (logic_block_size * ALIGNED_SIZE) / 1024;

    // the following size is in Kb
    _fs_size                = sb.filesystem_block_count * sig_block_size;
    _journaling_size        = sb.filesystem_journal_block_count * sig_block_size;
    _minimap_size           = sb.mini_bitmap_block_count * sig_block_size;
    _bitmap_size            = sb.bitmap_block_count * sig_block_size;
    _snapshot_history_size  = sb.snapshot_history_block_count * sig_block_size;
    _logic_size             = sb.logic_block_count * sb.logic_block_size * sig_block_size;

    printf("Filesystem basic information:\n"
           "      Super block structure size:         \t%lu bytes\n"
           "   *  Magic number:                       \t0x%lX\n"
           "   *  %1luKb block count:                    \t%lu (%s)\n"
           "  ==  Journaling starts from:             \t%lu\n"
           "      %1luKb journaling block count:         \t%lu (%s)\n"
           "  ==  Minibitmap starts from:             \t%lu\n"
           "      %1luKb Minibitmap block count:         \t%lu (%s)\n"
           "  ==  Bitmap starts from:                 \t%lu\n"
           "      %1luKb bitmap block count:             \t%lu (%s)\n"
           "  ==  Snapshot history starts from:       \t%lu\n"
           "      %1luKb snapshot history block count:   \t%lu (%s)\n"
           "  ==  Logic block starts from:            \t%lu\n"
           "      Logic block count:                  \t%lu (%s)\n"
           "      Logic block size (in %dKb):         \t%lu\n"
           "  **  Logic space efficiency:             \t%lf %%\n"
           "  **  Filesystem space efficiency:        \t%lf %%\n",
           sizeof (sb),
           sb.magic,
           sig_block_size,
           sb.filesystem_block_count, lltostr_with_scale(_fs_size).c_str(),
           sb.filesystem_journal_starting_block, sig_block_size,
           sb.filesystem_journal_block_count, lltostr_with_scale(_journaling_size).c_str(),
           sb.mini_bitmap_starting_block, sig_block_size,
           sb.mini_bitmap_block_count, lltostr_with_scale(_minimap_size).c_str(),
           sb.bitmap_starting_block, sig_block_size,
           sb.bitmap_block_count, lltostr_with_scale(_bitmap_size).c_str(),
           sb.snapshot_history_starting_block, sig_block_size,
           sb.snapshot_history_block_count, lltostr_with_scale(_snapshot_history_size).c_str(),
           sb.logic_block_starting_block,
           sb.logic_block_count, lltostr_with_scale(_logic_size).c_str(),
           (ALIGNED_SIZE) / 1024,
           sb.logic_block_size,
           (double)(sb.logic_block_size * sb.logic_block_count)
           / (double)sb.filesystem_block_count * 100.00,
           (double)(sb.logic_block_size * sb.logic_block_count
                    + sb.logic_block_starting_block)
           / (double)sb.filesystem_block_count * 100.00);
}

std::string lltostr_with_scale(uint64_t num)
{
    static char buffer [BLOCK_SIZE] { };
    std::string suffix = " ";
    uint64_t scale = 1;

    if (num < 1024)
    {
        suffix += "Kb";
    }
    else if (num < 1024 * 1024)
    {
        scale = 1024;
        suffix += "Mb";
    }
    else if (num < 1024 * 1024 * 1024)
    {
        scale = 1024 * 1024;
        suffix += "Gb";
    }
    else
    {
        scale = 1024 * 1024 * 1024;
        suffix += "Tb";
    }

    sprintf(buffer, "%.2Lf%s", num / ((long double)scale), suffix.c_str());

    return std::string(buffer);
}

void help(char ** argv)
{
    fprintf(stdout,     VERSION "\n"
            "Usage: %s [OPTIONS] DEVICE\n"
            "       -h, --help          Show this help message.\n"
            "       -k, --keep          Use current user's UID and GID for root inode.\n"
            "                           Default UID and GID for root inode is 0.\n"
            "       -b, --block-size    Specify logic block size. Must be multiples of %d Kb (%d).\n"
            "       -c, --cache-count   Filesystem cache count.\n"
            "       -m, --clear-bitmap  Clear filesystem bitmap.\n"
            "       -j, --clear-journal Clear journaling area.\n"
            "       -d, --direct-io     Bypass system cache.\n"
            ,
                    *argv,
                    BLOCK_SIZE / 1024,
                    BLOCK_SIZE);
}

void phrase_opt(int argc, char ** argv)
{
    int opt;
    int option_index;

    static struct option long_options[] =
            {
                    {"help",        no_argument,       nullptr, 'h'},
                    {"keep",        no_argument,       nullptr, 'k'},
                    {"block-size",  required_argument, nullptr, 'b'},
                    {"cache-count", required_argument, nullptr, 'c'},
                    {"clear-bitmap", no_argument, nullptr, 'm'},
                    {"direct-io",    no_argument, nullptr, 'd'},
                    {"clear-journal", no_argument, nullptr, 'j'},
                    {nullptr, 0, nullptr, 0}
            };

    while ((opt = getopt_long(argc, argv, "hb:c:kmdj", long_options, &option_index)) != -1)
    {
        switch (opt)
        {
            case 'b':
                logic_block_size = strtol(optarg, nullptr, 10);
                if (logic_block_size < (BLOCK_SIZE / ALIGNED_SIZE))
                {
                    fprintf(stderr, "[ERROR]: Block size is impossibly small!\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 'c':
                cache_space_size = strtol(optarg, nullptr, 10);
                if (cache_space_size == 0)
                {
                    fprintf(stderr, "[ERROR]: Empty cache is not allowed!\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 'h':
                help(argv);
                exit(EXIT_SUCCESS);
            case 'k':
                format_gid = getgid();
                format_uid = getuid();
                break;
            case 'm':
                clear_bitmap = true;
                break;
            case 'd':
                filesystem_runtime_flags.filesystem_direct_io = true;
                break;
            case 'j':
                clear_journaling = true;
                break;
            case '?':
                /* getopt_long already printed an error message. */
            default: /* '?' */
                help(argv);
                exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc)
    {
        fprintf(stderr, "Missing path to device after options.\n");
        help(argv);
        exit(EXIT_FAILURE);
    }

    device_name = argv[optind];
    // logic_block_size /= BLOCK_SIZE;
    // logic_block_size *= (BLOCK_SIZE / ALIGNED_SIZE); // in sector
}

int main(int argc, char ** argv)
{
    set_limit(BLOCK_SIZE * 64);

    phrase_opt(argc, argv);

    try
    {
        device filesystem;
        filesystem.set_bpoll_sz(cache_space_size);
        filesystem.open(device_name);

        // basic information
        uint64_t sector_count = filesystem._file_info.sectors;
        uint64_t journal_sector_count = sector_count * 0.003;
        if (journal_sector_count == 0)
        {
            journal_sector_count = 1;
        }

        uint64_t snapshot_history_sector_count = MAX_SNAPSHOT_VERSION / ALIGNED_SIZE
                + ((MAX_SNAPSHOT_VERSION % ALIGNED_SIZE == 0) ? 0 : 1);
        uint64_t remaining_sector_count = sector_count -
                journal_sector_count - snapshot_history_sector_count - 1 /* super block*/;
        uint64_t logic_block_count = (8 * remaining_sector_count * BLOCK_SIZE - 2 * BLOCK_SIZE)
                / (8 * logic_block_size * BLOCK_SIZE + BLOCK_SIZE + 1);
        uint64_t bitmap_sector_count = (logic_block_count + 1) / 8;
        uint64_t mini_bitmap_sector_count = logic_block_count / (8 * ALIGNED_SIZE)
                + (logic_block_count / (8 * ALIGNED_SIZE) == 0 ? 1 : 0);

        // super block
        super_block_t sb =
                {
                .magic = HMNXFS_MAGIC,
                .filesystem_block_count = sector_count / (BLOCK_SIZE / ALIGNED_SIZE),
                .filesystem_journal_starting_block = 1,
                .filesystem_journal_block_count = journal_sector_count / (BLOCK_SIZE / ALIGNED_SIZE),
                .magic_comp = ~HMNXFS_MAGIC,

                .last_mount_time{},
                .if_mount = 0,
                .allocated_logic_block_count = 0
                };

        sb.mini_bitmap_starting_block = sb.filesystem_journal_block_count + 1 /* super block */;
        sb.mini_bitmap_block_count = mini_bitmap_sector_count / (BLOCK_SIZE / ALIGNED_SIZE)
                                     + (mini_bitmap_sector_count % (BLOCK_SIZE / ALIGNED_SIZE) == 0 ? 0 : 1)
                                     + (mini_bitmap_sector_count == 0 ? 1 : 0);

        sb.bitmap_starting_block = sb.mini_bitmap_block_count + sb.mini_bitmap_starting_block;
        sb.bitmap_block_count = bitmap_sector_count / (BLOCK_SIZE / ALIGNED_SIZE)
                + (bitmap_sector_count % (BLOCK_SIZE / ALIGNED_SIZE) == 0 ? 0 : 1)
                + (bitmap_sector_count == 0 ? 1 : 0);

        sb.snapshot_history_starting_block = sb.bitmap_starting_block + sb.bitmap_block_count;
        sb.snapshot_history_block_count = snapshot_history_sector_count / (BLOCK_SIZE / ALIGNED_SIZE)
                + (snapshot_history_sector_count % (BLOCK_SIZE / ALIGNED_SIZE) == 0 ? 0 : 1)
                + (snapshot_history_sector_count == 0 ? 1 : 0);

        sb.logic_block_starting_block = sb.snapshot_history_starting_block + sb.snapshot_history_block_count;
        sb.logic_block_size = logic_block_size / (BLOCK_SIZE / ALIGNED_SIZE);
        sb.logic_block_count = logic_block_count;

        sb.allocated_logic_block_count = 3;
        sb.allocated_inode_count = 2;

        print_sb_info(sb);

        auto & super_block_buffer = filesystem.alloc_buffer(0);
        super_block_buffer.write((char*)&sb, sizeof (sb), 0);
        super_block_buffer.drop();

        char buff[BLOCK_SIZE] { };

        uint64_t display_base = 10;
        uint64_t filesystem_sz = sb.filesystem_block_count;
        while ((filesystem_sz / display_base) > 1000)
        {
            display_base *= 10;
        }

        // configuration
        // journaling:
        std::cout << "Clearing journaling ... " << std::flush;
        for (uint64_t i = 0; clear_journaling && i < sb.filesystem_journal_block_count; i++)
        {
            if (i % display_base == 0)
            {
                std::cout << i << ", " << std::flush;
            }

            auto &buffer = filesystem.alloc_buffer(sb.filesystem_journal_starting_block + i);
            buffer.write(buff, sizeof(buff), 0);
            buffer.drop();
        }
        std::cout << std::endl;


        // minibitmap:
        std::cout << "Clearing minibitmap ... " << std::flush;
        for (uint64_t i = 0; i < sb.mini_bitmap_block_count; i++)
        {
            if (i % display_base == 0)
            {
                std::cout << i << ", " << std::flush;
            }

            auto & buffer = filesystem.alloc_buffer(sb.mini_bitmap_starting_block + i);
            buffer.write(buff, sizeof (buff), 0);
            buffer.drop();
        }

        // set occupation status
        const char occupation_status = 0b00000111;
        auto & occupation_status_buffer = filesystem.alloc_buffer(sb.mini_bitmap_starting_block);
        occupation_status_buffer.write(&occupation_status, sizeof (occupation_status), 0);
        occupation_status_buffer.drop();

        std::cout << std::endl;


        // bitmap:
        std::cout << "Clearing bitmap ... " << std::flush;
        for (uint64_t i = 0; clear_bitmap && i < sb.bitmap_block_count; i++)
        {
            if (i % display_base == 0)
            {
                std::cout << i << ", " << std::flush;
            }

            auto &buffer = filesystem.alloc_buffer(sb.bitmap_starting_block + i);
            buffer.write(buff, sizeof(buff), 0);
            buffer.drop();
        }

        block_attribute_t empty_inode_attr
                {
            .block_type = BLOCK_DATA,
            .block_fs_flag = BLOCK_INODE,
                };

        // root inode:
        auto &empty_inode_attr_buffer = filesystem.alloc_buffer(sb.bitmap_starting_block);
        empty_inode_attr_buffer.write((char*)&empty_inode_attr, sizeof(empty_inode_attr), 0);
        // hmnx_cmd:
        empty_inode_attr_buffer.write((char*)&empty_inode_attr,
                                                   sizeof(empty_inode_attr),
                                                   sizeof(empty_inode_attr));
        empty_inode_attr_buffer.drop();
        std::cout << std::endl;


        // snapshot history:
        std::cout << "Clearing snapshot history ..." << std::endl;
        for (uint64_t i = 0; i < sb.snapshot_history_block_count; i++)
        {
            auto & buffer = filesystem.alloc_buffer(sb.snapshot_history_starting_block + i);
            buffer.write(buff, sizeof (buff), 0);
            buffer.drop();
        }


        std::cout << "Creating root ..." << std::endl;
        dentry_t root_dentry[3];

        root_dentry[0].inode_logic_location = 1;
        root_dentry[1].inode_logic_location = 1;
        root_dentry[2].inode_logic_location = 3;

        strcpy(root_dentry[0].name, ".");
        strcpy(root_dentry[1].name, "..");
        strcpy(root_dentry[2].name, ".hmnx_cmd");

        auto & dentry_buffer = filesystem.alloc_buffer(sb.logic_block_starting_block + sb.logic_block_size);
        dentry_buffer.write((char*)&root_dentry, sizeof (root_dentry), 0);
        dentry_buffer.drop();

        hmnx_inode root_inode = {
                .mode = S_IFDIR | 0755,
                .access_time = current_time(),
                .change_time = current_time(),
                .modify_time = current_time(),
                .file_len = 3 * sizeof (dentry_t),
                .inode_links = 2,
                .gid = format_gid,
                .uid = format_uid,
                .inode_direct_block_residence = 1,
                .inode_indirect_block_residence = 1,
        };

        root_inode.file_zone[0] = 2;

        hmnx_inode hmnx_cmd_inode = {
                .mode = S_IFREG | 0755,
                .access_time = current_time(),
                .change_time = current_time(),
                .modify_time = current_time(),
                .inode_links = 1,
                .gid = format_gid,
                .uid = format_uid,
                .inode_direct_block_residence = 3,
                .inode_indirect_block_residence = 3,
        };

            /* (inode is designed to fit in one basic buffer) */
        auto & buffer = filesystem.alloc_buffer(sb.logic_block_starting_block);
        buffer.write((char*)&root_inode, sizeof (root_inode), 0);
        buffer.drop();

        auto & buffer2 = filesystem.alloc_buffer(sb.logic_block_starting_block + sb.logic_block_size * 2);
        buffer2.write((char*)&hmnx_cmd_inode, sizeof (hmnx_cmd_inode), 0);
        buffer2.drop();

        std::cout << "Sync filesystem ..." << std::endl;

        filesystem.close();

        std::cout << "Filesystem formatted." << std::endl;

        if (sb.logic_block_size * sb.logic_block_count + sb.logic_block_starting_block > sb.filesystem_block_count)
        {
            errno = ENOSPC;
            str_throw_hmnx_error("Device is too small!");
        }

        if (sb.logic_block_count < 4096)
        {
            std::cerr << "[WARNING]: HMNX filesystem with insufficient logic blocks is going to be depleted rapidly!" << std::endl;
        }

        if (sb.filesystem_journal_block_count < 4)
        {
            std::cerr << "[WARNING]: HMNX filesystem with insufficient journaling space may not be able to store enough useful log!" << std::endl;
        }
    }
    catch (hmnx_error_t & err)
    {
        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        return EXIT_FAILURE;
    }


    return EXIT_SUCCESS;
}
