#include <shs/journal.h>
#include <shs/ll_io.h>
#include <shs/hmnx_error.h>
#include <cstdarg>
#include <shs/hmnx_time.h>

journal::journal(fd_t _fd, uint64_t _journal_starting_sector, uint64_t _journal_sector_size, bool recovery)
: journal_sector_size(_journal_sector_size), journal_starting_sector(_journal_starting_sector)
, fd(_fd), ignore_r_off(recovery)
{
    max_journal_entry = (_journal_sector_size * ALIGNED_SIZE) / sizeof (uint32_t) - START_OFFSET;
    buffer_size = (_journal_sector_size * ALIGNED_SIZE) / sizeof (uint32_t);
}

void journal::update_number(uint32_t val, uint64_t offset)
{
    uint64_t sector = (sizeof (uint32_t) * offset) / ALIGNED_SIZE;
    auto sector_offset = sizeof (uint32_t) * offset % ALIGNED_SIZE;
    char buff [ALIGNED_SIZE];
    ll_read(fd, buff, sector + journal_starting_sector);
    memcpy(buff + sector_offset, &val, sizeof (val));
    ll_write(fd, buff, sector + journal_starting_sector);
}

uint32_t journal::read_number(uint64_t offset) const
{
    uint32_t val;
    uint64_t sector = (sizeof (uint32_t) * offset) / ALIGNED_SIZE;
    auto sector_offset = sizeof (uint32_t) * offset % ALIGNED_SIZE;
    char buff [ALIGNED_SIZE];
    ll_read(fd, buff, sector + journal_starting_sector);
    memcpy(&val, buff + sector_offset, sizeof (val));
    return val;
}

uint32_t journal::read_offset()
{
    return read_number(buffer_size - DOFF_READ_OFFSET);
}

void journal::update_r_offset(uint32_t offset)
{
    update_number(offset, buffer_size - DOFF_READ_OFFSET);
}

uint32_t journal::write_offset()
{
    return read_number(buffer_size - DOFF_WRITE_OFFSET);
}

void journal::update_w_offset(uint32_t offset)
{
    update_number(offset, buffer_size - DOFF_WRITE_OFFSET);
}

uint32_t journal::null_point()
{
    return read_number(buffer_size - DOFF_NULL_POINT_OFFSET);
}

void journal::update_n_point(uint32_t val)
{
    update_number(val, buffer_size - DOFF_NULL_POINT_OFFSET);
}

void journal::push(uint32_t operation)
{
    auto w_off = write_offset();
    auto r_off = read_offset();
    auto n_poi = null_point();

    if (w_off < max_journal_entry - 1)
    {
        update_number(operation, w_off + 1); // write(ops, w+1)
        update_w_offset(w_off + 1);          // w += 1

        if (w_off == r_off && n_poi == NULL_POINT_ACTIVE)
        {
            update_r_offset(r_off + 1);      // r += 1
        }
    }
    else
    {
        update_w_offset(0);
        update_n_point(n_poi == NULL_POINT_INACTIVE ? NULL_POINT_ACTIVE : NULL_POINT_INACTIVE);
        update_number(operation, 0);
    }
}

uint32_t journal::pop(bool erase)
{
    auto w_off = write_offset();
    auto r_off = read_offset();
    auto n_poi = null_point();
    uint32_t ret;

    if (ignore_r_off)
    {
        if (!inited_native_r_off)
        {
            native_n_point = n_poi;
            native_r_off = r_off;
            inited_native_r_off = true;
        }
        else
        {
            n_poi = native_n_point;
            r_off = native_r_off;
        }
    }

    if (r_off == w_off && n_poi == NULL_POINT_INACTIVE)
    {
        code_throw_hmnx_error(EMPTY_BUFFER);
    }

    if (r_off < max_journal_entry - 1)
    {
        ret = read_number(r_off + 1);
        if (erase)
        {
            if (ignore_r_off)
            {
                native_r_off++;
            }
            else
            {
                update_r_offset(r_off + 1);
            }
        }
    }
    else
    {
        ret = read_number(0);
        if (erase)
        {
            if (ignore_r_off)
            {
                native_r_off = 0;
                native_n_point = !native_n_point;
            }
            else
            {
                update_r_offset(0);
                update_n_point(!n_poi);
            }
        }
    }

    return ret;
}

void journal::command(uint32_t cmd, ...)
{
    va_list valist;
    va_start(valist, cmd);

    push(cmd);
    push(~cmd);

    uint8_t parm_len = (cmd & 0xF0000000) >> 28;

    for (int i = 0; i < parm_len; i++)
    {
        push(va_arg(valist, uint32_t));
    }

    auto cur_time = current_time();
    push(GET_H32_OF_UINT64(cur_time.tv_sec));
    push(GET_L32_OF_UINT64(cur_time.tv_sec));
    push(GET_H32_OF_UINT64(cur_time.tv_nsec));
    push(GET_L32_OF_UINT64(cur_time.tv_nsec));

    va_end(valist);
}

void journal::clear()
{
    update_r_offset(0);
    update_w_offset(0);
    update_n_point(0);
}
