#include <shs/simple_bitmap.h>
#include <shs/hmnx_error.h>
#include <cstring>

// ALL bit count starts from 0

void simple_bitmap::init(uint8_t * _buffer, size_t num)
{
    if (bits)
    {
        str_throw_hmnx_error("Duplicated buffer initialization");
    }

    count = num;
    bits = _buffer;
    memset(bits, 0, num / 8 + (num % 8 == 0 ? 0 : 1));
}

simple_bitmap::~simple_bitmap()
{
    bits = nullptr;
}

bool simple_bitmap::get(size_t bit) const
{
    if (!bits)
    {
        str_throw_hmnx_error("Bitmap not initialized!");
    }

    if (bit >= count)
    {
        code_throw_hmnx_error(INVALID_INDEX);
    }

    auto byte_offset = bit / 8;
    auto bit_offset = bit % 8;
    uint8_t byte = bits[byte_offset];
    uint8_t pair = 0x01 << bit_offset;
    return byte & pair;
}

void simple_bitmap::set(size_t bit, bool val)
{
    if (!bits)
    {
        str_throw_hmnx_error("Bitmap not initialized!");
    }

    if (bit >= count)
    {
        code_throw_hmnx_error(INVALID_INDEX);
    }

    auto byte_offset = bit / 8;
    auto bit_offset = bit % 8;
    uint8_t byte = bits[byte_offset];
    uint8_t null_pair = ~(0x01 << bit_offset);
    uint8_t pair = (val ? 1 : 0) << bit_offset;
    byte &= null_pair;  // delete that bit
    byte |= pair;       // reset that bit
    bits[byte_offset] = byte; // set in bitmap
}

size_t simple_bitmap::next_free_node()
{
    if (!bits)
    {
        str_throw_hmnx_error("Bitmap not initialized!");
    }

    if (last_allocated_free_node == count - 1)
    {
        last_allocated_free_node = 0;
    }

    try
    {
        if (!get(last_allocated_free_node))
        {
            return last_allocated_free_node;
        }
        else if (!get(last_allocated_free_node + 1))
        {
            return ++last_allocated_free_node;
        }
    }
    catch (hmnx_error_t & err)
    {
        if (err.getx_errcode() != INVALID_INDEX)
        {
            throw ;
        }
    }
    catch (...)
    {
        throw;
    }

    last_allocated_free_node = 0;
    for (size_t i = 0; i < count; i++)
    {
        if (!get(i))
        {
            last_allocated_free_node = i;
            return i;
        }
    }

    code_throw_hmnx_error(BUFFER_DEPLETED);
}
