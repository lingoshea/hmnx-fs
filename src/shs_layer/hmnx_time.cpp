#include <shs/hmnx_time.h>
#include <unistd.h>
#include <chrono>

void hmnx_usleep(unsigned int usec)
{
    usleep(usec);
}

struct timespec current_time()
{
    struct timespec ts{};
    timespec_get(&ts, TIME_UTC);
    return ts;
}
