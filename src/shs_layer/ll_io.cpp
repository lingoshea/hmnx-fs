#include <shs/ll_io.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <shs/hmnx_error.h>
#include <string>
#include <cstring>
#include <fs.h>

void ll_read(fd_t fd, char * buffer, size_t sec)
{
    char aligned_buffer [ALIGNED_SIZE] __attribute__ ((__aligned__ (ALIGNED_SIZE))) { };
    // [WARNING]: data narrowing from size_t to long
    if (lseek(fd, (off_t)sec * ALIGNED_SIZE, SEEK_SET) == -1)
    {
        str_throw_hmnx_error("Low-level lseek failed, fd=" + std::to_string(fd));
    }

    ssize_t ret = read(fd, aligned_buffer, ALIGNED_SIZE);
    if (ret == ALIGNED_SIZE)
    {
        memcpy(buffer, aligned_buffer, ALIGNED_SIZE);
        return;
    }

    str_throw_hmnx_error("Low-level read failed, fd=" + std::to_string(fd));
}

void ll_write(fd_t fd, const char * buffer, size_t sec)
{
    char aligned_buffer [ALIGNED_SIZE] __attribute__ ((__aligned__ (ALIGNED_SIZE))) { };
    // [WARNING]: data narrowing from size_t to long
    if (lseek(fd, (off_t)sec * ALIGNED_SIZE, SEEK_SET) == -1)
    {
        str_throw_hmnx_error("Low-level lseek failed, fd=" + std::to_string(fd));
    }

    memcpy(aligned_buffer, buffer, ALIGNED_SIZE);
    ssize_t ret = write(fd, aligned_buffer, ALIGNED_SIZE);
    if (ret == ALIGNED_SIZE)
    {
        return;
    }

    str_throw_hmnx_error("Low-level write failed, fd=" + std::to_string(fd));
}

file_info_t ll_open(const char * filename)
{
    file_info_t file_info {};
    off64_t file_size;
    fd_t fd = 0;

    if (filesystem_runtime_flags.filesystem_direct_io)
    {
        fd = open(filename, O_RDWR | O_DIRECT);
    }
    else
    {
        fd = open(filename, O_RDWR);
    }

    if (fd > 0)
    {
        file_info.fd = fd;
    }
    else
    {
        str_throw_hmnx_error("Cannot open file " + std::string(filename));
    }

    if ((file_size = lseek(fd, 0, SEEK_END)) == -1)
    {
        str_throw_hmnx_error("Low-level lseek failed, fd=" + std::to_string(fd));
    }

    file_info.sectors = file_size / ALIGNED_SIZE;

    return file_info;
}

void ll_close(fd_t fd)
{
    close(fd);
}
