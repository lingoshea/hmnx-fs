#include <shs/device.h>
#include <cstring>
#include <shs/hmnx_error.h>
#include <shs/hmnx_time.h>

void device::set_bpoll_sz(size_t pool_size)
{
    try
    {
        if (!pool_size)
        {
            str_throw_hmnx_error("Cache space cannot be 0");
        }

        _raw_buffer_pool = new sbuffer[pool_size];
        auto size = pool_size / 8 + (pool_size % 8 == 0 ? 0 : 1);
        _bitmap_buffer = new uint8_t [size];
        buffer_bitmap.init(_bitmap_buffer, pool_size);
    }
    catch (...)
    {
        str_throw_hmnx_error("Device initialization failed!");
    }
}

void device::open(const std::string & path)
{
    halt_operation = 0;
    allocating_wait = 0;
    file_info = ll_open(path.c_str());
}

void device::close()
{
    halt_operation = 1;
    if (file_info.fd)
    {
        free_buffer(access_list.size(), true);
        ll_close(file_info.fd);
        memset(&file_info, 0, sizeof (file_info));
    }
    halt_operation = 0;
}

sbuffer &device::alloc_buffer(size_t block)
{
    if (halt_operation)
    {
        code_throw_hmnx_error(OPERATION_HALTED);
    }

    if (block >= file_info.sectors / (BLOCK_SIZE / ALIGNED_SIZE))
    {
        code_throw_hmnx_error(INVALID_INDEX);
    }

    // check if the buffer is in active pool
    auto result = buffer_pool.find(block);
    if (result != buffer_pool.end())
    {
        // buffer is already allocated in the cache pool
        result->second->ops_dropped = 0;
        return *result->second;
    }

    while (allocating_wait)
        hmnx_usleep(1000);

    allocating_wait = 1;

    // if not, then we allocate this buffer and add it to the active buffer pool
    auto buffer_num = next_free_buffer();
    buffer_bitmap.set(buffer_num, true);
    access_list.emplace_back(block);
    buffer_pool.emplace(block, &_raw_buffer_pool[buffer_num]);
    auto & buffer = _raw_buffer_pool[buffer_num];
    buffer.ops_dropped = 0;
    buffer.ops_invoked = 0;
    buffer.index = buffer_num;
    buffer.file_descriptor = file_info.fd;
    buffer.block_num = block;
    buffer.write_in_progress = 0;
    buffer.dirty = 0;

    allocating_wait = 0;

    return _raw_buffer_pool[buffer_num];
}

size_t device::next_free_buffer()
{
    int8_t retry_timeout = 3;
    retry:
    try
    {
        return buffer_bitmap.next_free_node();
    }
    catch (hmnx_error_t & err)
    {
        if (err.getx_errcode() == BUFFER_DEPLETED && retry_timeout > 0)
        {
            free_buffer(access_list.size() / 2);
            hmnx_usleep(1000);
            retry_timeout--;
            goto retry;
        }

        throw ;
    }
    catch (...)
    {
        throw ;
    }
}

void device::sync()
{
    if (_fs_journal) {
        _fs_journal->command(FILESYSTEM_SYNC);
    }

    free_buffer(access_list.size());

    if (_fs_journal) {
        _fs_journal->command(OPERATION_FINISHED);
    }
}

void device::free_buffer(size_t size, bool force)
{
    try
    {
        if (access_list.empty())
        {
            return;
        }

        halt_operation = 1;

        if (size == 0)
        {
            size = 1;
        }

        if (size > access_list.size())
        {
            size = access_list.size();
        }

        auto it = access_list.begin();

        for (size_t i = 0; i < size; i++)
        {
            sbuffer &buffer = *buffer_pool.at(*it);

            if (buffer.is_dropped() || force)
            {
                if (buffer.dirty)
                {
                    if (_fs_journal)
                    {
                        _fs_journal->command(CHECKSUM_IN_BLOCK,
                                             MK_64bit_PARAM(buffer.block_num),
                                             MK_64bit_PARAM(buffer.checksum));
                    }

                    // write data to disk
                    size_t offset = (*it) * (BLOCK_SIZE / ALIGNED_SIZE);

                    for (size_t block = 0; block < (BLOCK_SIZE / ALIGNED_SIZE); block++)
                    {
                        ll_write(file_info.fd, buffer.data + block * ALIGNED_SIZE, offset + block);
                    }
                }

                // erase buffer
                buffer_bitmap.set(buffer.index, false);
                buffer_pool.erase(*it);
                access_list.erase(it);
            }
            else
            {
                it++;
            }
        }
    }
    catch (...)
    {
        halt_operation = 0;
        throw ;
    }

    halt_operation = 0;
}

device::~device()
{
    close();

    delete []_bitmap_buffer;
    delete []_raw_buffer_pool;

    _raw_buffer_pool = nullptr;
    _bitmap_buffer = nullptr;
}

device::device()
{
    halt_operation = 1;
    allocating_wait = 0;
}
