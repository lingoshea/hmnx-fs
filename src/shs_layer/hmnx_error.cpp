#include <cerrno>
#include <shs/hmnx_error.h>
#include <sstream>
#include <cstring>
#include <execinfo.h>

#define BACKTRACE_SZ 64

const char * hmnx_error_t::what() const noexcept
{
    return error.c_str();
}

hmnx_error_t::hmnx_error_t(const std::string& str,
                                 const std::string& filename,
                                 const std::string& function,
                                 unsigned long long line,
                                 void * parent,
                                 uint32_t additional_error_code) noexcept
{
    std::stringstream sserror;
    void *array[BACKTRACE_SZ];
    char **strings;
    int size, i;
    bool output_color   = false;
    const char * term   = secure_getenv("TERM");
    const char * red    = "\033[01;31m";
    const char * green  = "\033[01;32m";
    const char * yellow = "\033[01;33m";
    const char * blue   = "\033[01;34m";
    const char * purple = "\033[01;35m";
    const char * l_blue = "\033[01;36m";
    const char * l_white  = "\033[01;37m";
    const char * no_color = "\033[0m";

    if (term && !strcmp(term, "xterm-256color"))
    {
        output_color = true;
    }

    _state_snapshot._errno = errno;
    _state_snapshot.parent = parent;
    _state_snapshot.filename = filename;
    _state_snapshot.function = function;
    _state_snapshot.line = line;
    _state_snapshot.additional_error_code = additional_error_code;

    size = backtrace (array, BACKTRACE_SZ);
    strings = backtrace_symbols (array, size);
    if (strings != nullptr)
    {
        sserror << "\n<== !! Exception occurred !! ==>\nObtained stack frame(s):\n";

        for (i = 0; i < size; i++)
        {
            if (output_color)
            {
                sserror << green;
            }

            sserror << strings[i] << "\n";

            if (output_color)
            {
                sserror << no_color;
            }
        }
    }

    free (strings);

    sserror << "From ";
    if (output_color) {
        sserror << l_blue;
    }
    sserror << _state_snapshot.filename;
    if (output_color) {
        sserror << no_color;
    }
    sserror << ":";
    if (output_color) {
        sserror << purple;
    }
    sserror << _state_snapshot.line;
    if (output_color) {
        sserror << no_color;
    }
    sserror << ":";
    if (output_color) {
        sserror << l_white;
    }
    sserror << _state_snapshot.function;
    if (output_color) {
        sserror << no_color;
    }
    sserror << ": ";

    if (output_color) {
        sserror << red;
    }
    sserror << str;

    if (output_color) {
        sserror << no_color;
    }
    sserror << " (errno=\"";

    if (output_color) {
        sserror << yellow;
    }
    sserror << strerror(errno);

    if (output_color) {
        sserror << no_color;
    }

    sserror << "\")\n" << "<== End of exception log ==>\n";

    error = sserror.str();
}

int hmnx_error_t::get_errno() const
{
    return _state_snapshot._errno;
}

uint32_t hmnx_error_t::getx_errcode() const
{
    return _state_snapshot.additional_error_code;
}

