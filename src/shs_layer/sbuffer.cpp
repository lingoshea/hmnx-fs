#include <shs/hmnx_error.h>
#include <shs/sbuffer.h>
#include <cstring>
#include <sys/resource.h>
#include <xxhash64.h>
#include <shs/hmnx_time.h>

dbuffer_len_t sbuffer::read(char * buffer, dbuffer_len_t len, dbuffer_off_t off)
{
    // sanity check:
    if (ops_dropped)
    {
        code_throw_hmnx_error(INVALID_BUFFER);
    }

    if (off >= buffer_size)
    {
        return 0;
    }

    refresh_from_disk();

    if ((len + off) > buffer_size)
    {
        len = buffer_size - off;
    }

    // operations:
    memcpy(buffer, data + off, len);
    return len;
}

dbuffer_len_t sbuffer::write(const char * buffer, dbuffer_len_t len, dbuffer_off_t off)
{
    // sanity check:
    if (ops_dropped)
    {
        code_throw_hmnx_error(INVALID_BUFFER);
    }

    if (off >= buffer_size)
    {
        return 0;
    }

    while (write_in_progress)
        hmnx_usleep(1000);

    write_in_progress = 1;

    // skip read if the entire block is being overwritten
    if (len != BLOCK_SIZE)
    {
        refresh_from_disk();
    }

    ops_invoked = 1;

    if ((len + off) > buffer_size)
    {
        len = buffer_size - off;
    }

    dirty = 1;

    // operations:
    memcpy(data + off, buffer, len);
    checksum = XXHash64::hash(data, BLOCK_SIZE, HMNXFS_CHECKSUM_SEED);

    write_in_progress = 0;
    return len;
}

void sbuffer::refresh_from_disk()
{
    auto raw_block_num = block_num * (buffer_size / ALIGNED_SIZE);
    if (!ops_invoked)
    {
        for (int i = 0; i < (buffer_size / ALIGNED_SIZE); i++)
        {
            ll_read(file_descriptor, data + ALIGNED_SIZE * i, raw_block_num + i);
        }
    }

    ops_invoked = 1;
}

void set_limit(unsigned long kStackSize)
{
    struct rlimit rl{};
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                str_throw_hmnx_error("setrlimit failed");
            }
        }
    }
}
